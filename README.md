# Travolute Drupal module
## Setup
1. Install the module with `composer require 'drupal/travolute'`
2. Clear your cache
3. Enter your Api key and secret at Configuration > Development > Travolute Settings

Warning: This configuration is shared across environments when exported and imported. To prevent this,
see Environment variables below.

## Environment variables
The module also offers the possibility to use environment variables. If the TRAVOLUTE_API_KEY environment
variable is available, this will be used instead of the configuration provided above.
The following environment variables can be set:
- TRAVOLUTE_WSDL
- TRAVOLUTE_API_KEY
- TRAVOLUTE_API_SECRET
