<?php


namespace Drupal\travolute\tests\Unit;


use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Drupal\travolute\Controller\FilterController;
use Drupal\travolute\Model\FilterDataResponse;
use Drupal\travolute\ValueObject\FilterQueryParameters;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FilterControllerTest
 *
 * @group travolute-unit
 */
class FilterControllerTest extends UnitTestCase
{
    private $mockedTravoluteService;
    private $response;
    private $queryData;
    private $request;
    private $filterResponse;
    private $filters;

    protected function setUp()
    {
        parent::setUp();
        /** @var TravoluteServiceInterface|MockObject $mockedTravoluteService */
        $this->mockedTravoluteService = $this->getMockBuilder(TravoluteService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->response = array(
            'airports' => [],
            'regions' => [],
            'cities' => [],
            'countries' => [],
            'durations' => [],
        );

        $this->queryData = [
            'items' => 'FILTERDATA_TYPE_AIRPORT',
            'country' => 227,
            'region' => 0,
            'city' => 2,
            'airport' => 0,
        ];

        $this->request = new Request($this->queryData);

        $this->filterResponse = new FilterDataResponse($this->response);

        $this->filters = (new FilterQueryParameters($this->queryData))->toArray();
    }

    /**
     * Check if structure of filters response meets expectations
     */
    public function testGetResponse()
    {
        $this->mockedTravoluteService->method('getFilters')
            ->with($this->filters[0], $this->filters[1])
            ->willReturn($this->filterResponse);

        $controller = new FilterController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse((new FilterDataResponse($this->response))->jsonSerialize()),
            $controller->get($this->request)
        );
    }

    public function testSoapFault()
    {
        $this->mockedTravoluteService->method('getFilters')
            ->with($this->filters[0], $this->filters[1])
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $controller = new FilterController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse(['error' => 'Error message'], 500),
            $controller->get($this->request)
        );
    }
}
