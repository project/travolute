<?php


namespace Drupal\travolute\tests\Unit;


use Carbon\Carbon;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Model\AccommodationDetailResponse;
use Drupal\travolute\Model\AutoSuggestResponse;
use Drupal\travolute\Model\FilterDataResponse;
use Drupal\travolute\Model\LoginResponse;
use Drupal\travolute\Model\PriceAvailabilityResponse;
use Drupal\travolute\Model\SearchResponse;
use Drupal\travolute\ValueObject\AccommodationParameters;
use Drupal\travolute\ValueObject\Booking;
use Drupal\travolute\ValueObject\SearchQueryParameters;

/**
 * Class TravoluteServiceTest
 *
 * @group travolute-unit
 */
class TravoluteServiceTest extends UnitTestCase
{
    private $mockedSoapClient;
    private $mockedCache;
    private $cacheCarbon;

    protected function setUp()
    {
        parent::setUp();
        $this->mockedSoapClient = $this->getMockFromWsdl('http://api.travolute.netants.nl/soap?wsdl');

        $this->mockedCache = $this->getMockBuilder(CacheBackendInterface::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->cacheCarbon = (object)array("data" => (new Carbon())->addDays(3));

        $this->mockedCache->expects($this->any())
            ->method('get')
            ->with($this->logicalOr(
                $this->equalTo('auth_token'),
                $this->equalTo('regenerate_token'),
                $this->equalTo('auth_token_expire')
            ))
            ->willReturn($this->cacheCarbon);
    }

    public function testLogin()
    {
        $newExpire = (new Carbon())->addCentury(1)->getTimestamp();
        $response = (object)array(
            "auth_token" => "token",
            "auth_token_expire" => $newExpire,
            "regenerate_token" => "regenToken",
        );

        $loginResponse = new LoginResponse(
            $response->auth_token,
            $response->auth_token_expire,
            $response->regenerate_token
        );

        $this->mockedSoapClient->method('login')
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->assertEquals($loginResponse, $service->login());
    }

    public function testLoginFault()
    {
        $this->mockedSoapClient->method('login')
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->login();
    }

    public function testAutoSuggest()
    {
        $response = (object)array(
            "items" => array(
                (object)array(
                    'name' => 'test',
                    'code' => 100256,
                    'stars' => 3,
                    'images' => array((object)array('url' => 'test')),
                    'destination' => (object)array(
                        'id' => 1,
                        'city' => 'Roermond',
                        'countryId' => 227,
                        'country' => 'Turkije',
                    ),
                    'accommodationFacts' => [],  // TODO: Fill this array with AccomodationFacts
                    'lastGiataCheck' => 4,
                    'accommodationTexts' => array(
                        (object)array(
                            'textTitle' => 'test',
                            'textPara' => 'test',
                            'textLanguage' => 'test',
                        )
                    ),
                )
            ),
            "responseCount" => 0
        );

        $autoSuggestResponse = new AutoSuggestResponse($response);

        $this->mockedSoapClient->method('autosuggest')
            ->with(['query' => 'test'])
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->assertEquals($autoSuggestResponse, $service->autoSuggest('test'));
    }

    public function testAutoSuggestFault()
    {
        $this->mockedSoapClient->method('autosuggest')
            ->with(['query' => 'test'])
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->autoSuggest('test');
    }


    public function testGetFilters()
    {
        $response = array(
            "airports" => array(),
            "regions" => array(),
            "cities" => array(),
            "countries" => array(),
            "boardtypes" => array(),
            "durations" => array(),
        );

        $filterDataResponse = new FilterDataResponse($response);

        $this->mockedSoapClient->method('getFilterData')
            ->with(['test'], ['test'])
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->assertEquals($filterDataResponse, $service->getFilters(['test'], ['test']));
    }

    public function testGetFiltersFault()
    {
        $this->mockedSoapClient->method('getFilterData')
            ->with(['test'], ['test'])
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->getFilters(['test'], ['test']);
    }

    public function testSearch()
    {
        $response = (object)array(
            "items" => array(),
            "responseCount" => 0
        );

        $searchQueryParams = new SearchQueryParameters([
            'departure_date' => '2019-12-20',
            'query' => 'riu%20palace',
            'departure_offset' => 0,
            'occupancy' => 2,
        ]);

        $searchResponse = new SearchResponse($response);

        $this->mockedSoapClient->method('search')
            ->with($searchQueryParams->toArray())
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->assertEquals($searchResponse, $service->search($searchQueryParams));
    }

    public function testSearchFault()
    {
        $searchQueryParams = new SearchQueryParameters([
            'departure_date' => '2019-12-20',
            'query' => 'riu%20palace',
            'departure_offset' => 0,
            'occupancy' => 2,
        ]);

        $this->mockedSoapClient->method('search')
            ->with($searchQueryParams->toArray())
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->search($searchQueryParams);
    }

    public function testDetail()
    {
        $response = (object) array(
            'accommodation' => (object) array(
                'name' => 'test',
                'code' => 100256,
                'stars' => 3,
                'accommodationImages' => array( (object) array('url' => 'test') ),
                'destination' => (object) array(
                    'id' => 1,
                    'city' => 'Roermond',
                    'countryId' => 227,
                    'country' => 'Turkije',
                ),
                'accommodationFacts' => [],   // TODO: Fill this array with AccomodationFacts
                'lastGiataCheck' => 4,
                'accommodationTexts' => array(
                    (object) array(
                        'textTitle' => 'test',
                        'textPara' => 'test',
                        'textLanguage' => 'test',
                    )
                ),
                'prices' => [],
            ),
            'filters' => [],
        );

        $accoDetailResponse = new AccommodationDetailResponse($response);
        $searchQueryParams = new AccommodationParameters([
            'occupancy' => 2,
            'departure_date' => '2019-12-12',
            'return_date' => '2019-12-19',
            'airport' => [2],
            'duration' => [8,9,10],
            'boardType' => []
        ]);

        $this->mockedSoapClient->method('detailAccommodation')
            ->with(10, $searchQueryParams->toArray())
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->assertEquals($accoDetailResponse, $service->detail(10, $searchQueryParams));
    }

    public function testDetailFault()
    {
        $searchQueryParams = new AccommodationParameters([
            'occupancy' => 2,
            'departure_date' => '2019-12-12',
            'return_date' => '2019-12-19',
            'airport' => [2],
            'duration' => [8,9,10],
            'boardType' => []
        ]);
        $this->mockedSoapClient->method('detailAccommodation')
            ->with(10, $searchQueryParams->toArray())
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->detail(10, $searchQueryParams);
    }

    public function testAvailability()
    {
        $response = (object) array(
            "bookingCode" => "test",
            "priceAvailabilityModel" => (object) array(
                "priceDetails" => array( (object) array(
                    "description" => "test",
                    "amount" => "test",
                    "type" => "test",
                    "empty" => "test",
                    "price" => "test",
                )),
                "available" => "test",
                "fromFlightModel" => (object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                ),
                "toFlightModel" => (object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                ),
                "toFlightModelList" => array((object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                )),
                "fromFlightModelList" => array((object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                )),
                "total_price" => 'test',
                "errata" => 'test',
                "error_response" => 'test',
            ),
        );

        $availabilityResponse = new PriceAvailabilityResponse($response);

        $this->mockedSoapClient->method('checkAvailability')
            ->with("key", ["customers"])
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->assertEquals($availabilityResponse, $service->availability("key", ["customers"]));
    }

    public function testAvailabilityFault()
    {
        $this->mockedSoapClient->method('checkAvailability')
            ->with("key", ["customers"])
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->availability("key", ["customers"]);
    }

    public function testEnsureLoggedIn()
    {
        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);
        $service->ensureLoggedIn();
        $this->assertEquals($this->cacheCarbon->data, $service->getAuthToken());
        $this->assertEquals($this->cacheCarbon->data, $service->getAuthTokenExpire());
        $this->assertEquals($this->cacheCarbon->data, $service->getRegenerateToken());
    }

    public function testPreBooking()
    {
        $response = (object) array(
            "id" => "test",
            "code" => "test",
            "created_at" => "test",
            "updated_at" => "test",
            "accommodationId" => "test",
            "departureDate" => "test",
            "numberOfDays" => "test",
            "roomTypeId" => "test",
            "boardTypeId" => "test",
            "occupancy" => "test",
            "departureAirportId" => "test",
            "customers" => "test",
            "priceModel" => "test",
            "priceAvailabilityModel" => "test",
            "externalCode" => "test",
        );

        $bookingCode = 'code';

        $this->mockedSoapClient->method('preBooking')
            ->with($bookingCode)
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);
        $this->assertEquals(new Booking($response), $service->preBooking($bookingCode));
    }

    public function testPreBookingFault()
    {
        $bookingCode = 'code';

        $this->mockedSoapClient->method('preBooking')
            ->with($bookingCode)
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->preBooking($bookingCode);
    }

    public function testBooking()
    {
        $response = (object) array(
            "id" => "test",
            "code" => "test",
            "created_at" => "test",
            "updated_at" => "test",
            "accommodationId" => "test",
            "departureDate" => "test",
            "numberOfDays" => "test",
            "roomTypeId" => "test",
            "boardTypeId" => "test",
            "occupancy" => "test",
            "departureAirportId" => "test",
            "customers" => "test",
            "priceModel" => "test",
            "priceAvailabilityModel" => "test",
            "externalCode" => "test",
        );

        $bookingCode = 'code';
        $customers = ["customers"];

        $this->mockedSoapClient->method('booking')
            ->with($bookingCode, $customers)
            ->willReturn($response);

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);
        $this->assertEquals(new Booking($response), $service->booking($bookingCode, $customers));
    }

    public function testBookingFault()
    {
        $bookingCode = 'code';
        $customers = ["customers"];

        $this->mockedSoapClient->method('booking')
            ->with($bookingCode, $customers)
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $service = new TravoluteService('key', 'secret', $this->mockedSoapClient, $this->mockedCache);

        $this->expectException(\SoapFault::class);
        $this->expectExceptionMessage('Error message');
        $service->booking($bookingCode, $customers);
    }

}
