<?php

namespace Drupal\travolute\tests\Unit;

use \JsonException;
use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Controller\BookingController;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Drupal\travolute\ValueObject\Booking;
use PHPUnit\Framework\MockObject\MockObject;
use SoapFault;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class BookingControllerTest
 *
 * @group travolute-unit
 */
class BookingControllerTest extends UnitTestCase
{
  /** @var TravoluteServiceInterface|MockObject $mockedTravoluteService */
  private $mockedTravoluteService;
  /** @var object */
  private $response;
  /** @var BookingController */
  private $controller;

  protected function setUp(): void
  {
    parent::setUp();

    $this->mockedTravoluteService = $this->getMockBuilder(TravoluteService::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->response = (object)[
      'id' => 123,
      'externalCode' => 'SomeExternalCode',
    ];
    $this->controller = new BookingController($this->mockedTravoluteService);
  }

  /**
   * @throws JsonException
   */
  public function testPreBooking(): void
  {
    // ARRANGE
    $postData = [
      'bookingCode' => 'abc123DEF',
    ];
    $this->mockedTravoluteService->method('preBooking')
      ->with($postData['bookingCode'])
      ->willReturn($this->response);

    // ACT
    $request = new Request([], [], [], [], [], [], json_encode($postData));

    // ASSERT
    $this->assertEquals(
      new JsonResponse((new Booking($this->response))->jsonSerialize()),
      $this->controller->preBooking($request)
    );
  }

  /**
   * @throws JsonException
   */
  public function testSoapFaultPreBooking(): void
  {
    // ARRANGE
    $postData = [
      'bookingCode' => 'abc123DEF',
    ];
    $this->mockedTravoluteService->method('preBooking')
      ->with($postData['bookingCode'])
      ->willThrowException(new SoapFault('SOAP-ENV:Server', 'Error message'));

    // ACT
    $request = new Request([], [], [], [], [], [], json_encode($postData));

    // ASSERT
    $this->assertEquals(
      new JsonResponse(['error' => 'Error message'], 500),
      $this->controller->preBooking($request)
    );
  }

  /**
   * @throws JsonException
   */
  public function testBooking(): void
  {
    // ARRANGE
    $postData = [
      'bookingCode' => 'abc123DEF',
      'customers' => [
        (object)[
          'lastname' => 'Last Name',
          'firstname' => 'Mister',
          'gender' => 'M',
        ],
        (object)[
          'lastname' => 'Last Name',
          'firstname' => 'Misses',
          'gender' => 'F',
        ],
      ]
    ];
    $this->mockedTravoluteService->method('booking')
      ->with($postData['bookingCode'], $postData['customers'])
      ->willReturn($this->response);

    // ACT
    $request = new Request([], [], [], [], [], [], json_encode($postData));

    // ASSERT
    $this->assertEquals(
      new JsonResponse((new Booking($this->response))->jsonSerialize()),
      $this->controller->booking($request)
    );
  }

  /**
   * @throws JsonException
   */
  public function testSoapFaultBooking(): void
  {
    // ARRANGE
    $postData = [
      'bookingCode' => 'abc123DEF',
      'customers' => [
        (object)[
          'lastname' => 'Last Name',
          'firstname' => 'Mister',
          'gender' => 'M',
        ],
        (object)[
          'lastname' => 'Last Name',
          'firstname' => 'Misses',
          'gender' => 'F',
        ],
      ]
    ];
    $this->mockedTravoluteService->method('booking')
      ->with($postData['bookingCode'], $postData['customers'])
      ->willThrowException(new SoapFault('SOAP-ENV:Server', 'Error message'));

    // ACT
    $request = new Request([], [], [], [], [], [], json_encode($postData));

    // ASSERT
    $this->assertEquals(
      new JsonResponse(['error' => 'Error message'], 500),
      $this->controller->booking($request)
    );
  }
}
