<?php


namespace Drupal\travolute\tests\Unit;


use Carbon\Carbon;
use DateTimeZone;
use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Drupal\travolute\Controller\AccommodationController;
use Drupal\travolute\Controller\AccommodationsAutoSuggestController;
use Drupal\travolute\Model\Accommodation;
use Drupal\travolute\Model\AccommodationDetailResponse;
use Drupal\travolute\Model\AccommodationTextCollection;
use Drupal\travolute\Model\Country;
use Drupal\travolute\Model\Destination;
use Drupal\travolute\Model\Image;
use Drupal\travolute\Model\ImageCollection;
use Drupal\travolute\Model\Region;
use Drupal\travolute\ValueObject\AccommodationParameters;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationControllerTest
 *
 * @group travolute-unit
 */

class AccommodationControllerTest extends UnitTestCase
{
    private $mockedTravoluteService;
    private $response;
    private $queryData;
    private $request;
    private $accommodationResponse;
    private $filters;

    protected function setUp()
    {
        parent::setUp();
        /** @var TravoluteServiceInterface|MockObject $mockedTravoluteService */
        $this->mockedTravoluteService = $this->getMockBuilder(TravoluteService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->response = (object) array(
            'accommodation' => (object) array(
                'name' => 'test',
                'code' => 100256,
                'stars' => 3,
                'accommodationImages' => array( (object) array('url' => 'test') ),
                'destination' => (object) array(
                    'id' => 1,
                    'city' => 'Roermond',
                    'countryId' => 227,
                    'country' => 'Turkije',
                ),
                'accommodationFacts' => array(), // TODO: Fill this array with AccomodationFacts
                'lastGiataCheck' => 4,
                'accommodationTexts' => array(
                    (object) array(
                        'textTitle' => 'test',
                        'textPara' => 'test',
                        'textLanguage' => 'test',
                    )
                ),
                'prices' => [],
            ),
            'filters' => [],
        );

        $this->queryData = [
            'occupancy' => 2,
            'departure_date' => '2019-12-12',
            'return_date' => '2019-12-19',
            'airport' => [2],
            'duration' => [8,9,10],
            'boardType' => []
        ];

        $this->request = new Request([], [], [], [], [], [], json_encode($this->queryData));

        $this->accommodationResponse = new AccommodationDetailResponse($this->response);

        $this->filters = new AccommodationParameters($this->queryData);
    }

    /**
     * Check if structure of accommodation response meets expectations
     */
    public function testGetResponse()
    {
        $this->mockedTravoluteService->method('detail')
            ->with(100256, $this->filters)
            ->willReturn($this->accommodationResponse);

        $controller = new AccommodationController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse((new AccommodationDetailResponse($this->response))->jsonSerialize()),
            $controller->get(100256, $this->request)
        );
    }

    public function testSoapFault()
    {
        $this->mockedTravoluteService->method('detail')
            ->with(100256, $this->filters)
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $controller = new AccommodationController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse(['error' => 'Error message'], 500),
            $controller->get(100256, $this->request)
        );
    }
}
