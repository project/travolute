<?php

namespace Drupal\travolute\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Drupal\travolute\Controller\OfferController;
use Drupal\travolute\Model\SearchResponse;
use Drupal\travolute\ValueObject\SearchQueryParameters;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SearchQueryParametersTest
 *
 * @package Drupal\Tests\travolute\Functional
 * @group travolute-unit
 */
class SearchQueryParametersTest extends UnitTestCase
{
  public function parameterProvider(): array
  {
    return [
      'single-country' => [
        'constructorParams' => [
          'country' => '1'
        ],
        'toArray' => [
          'country' => [1],
        ],
      ],
      'multiple-countries' => [
        'constructorParams' => [
          'country' => '1,2'
        ],
        'toArray' => [
          'country' => [1,2],
        ],
      ],
      'no-countries' => [
        'constructorParams' => [
        ],
        'toArray' => [
        ],
      ],
    ];
  }

  /**
   * @param $params
   * @dataProvider parameterProvider
   */
  public function testSearchQueryParametersConstructor($constructorParams, $toArray): void
  {
    $searchQueryParameters = new SearchQueryParameters($constructorParams);
    $this->assertArrayEquals($toArray, $searchQueryParameters->toArray());

  }
}
