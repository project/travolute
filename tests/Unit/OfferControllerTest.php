<?php

namespace Drupal\travolute\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Drupal\travolute\Controller\OfferController;
use Drupal\travolute\Model\SearchResponse;
use Drupal\travolute\ValueObject\SearchQueryParameters;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class OfferControllerTest
 *
 * This is the functional test suite for testing the Travolute filters
 *
 * @package Drupal\Tests\travolute\Functional
 * @group travolute-unit
 */
class OfferControllerTest extends UnitTestCase
{
    private $mockedTravoluteService;
    private $response;
    private $queryData;
    private $request;
    private $searchResponse;

    protected function setUp()
    {
        parent::setUp();
        /** @var TravoluteServiceInterface|MockObject $mockedTravoluteService */
        $this->mockedTravoluteService = $this->getMockBuilder(TravoluteService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->response = (object) array(
            'items' => [],
            'responseCount' => 1
        );

        $this->queryData = [
            'departure_date' => '2019-12-20',
            'query' => 'riu%20palace',
            'departure_offset' => 0,
            'occupancy' => 2,
        ];

        $this->request = new Request($this->queryData);

        $this->searchResponse = new SearchResponse($this->response);
    }

    /**
     * Check if structure of offer response meets expectations
     */
    public function testGetResponse()
    {
        $this->mockedTravoluteService->method('search')
            ->with(new SearchQueryParameters($this->queryData), 0)
            ->willReturn($this->searchResponse);

        $controller = new OfferController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse((new SearchResponse($this->response))->jsonSerialize()),
            $controller->get($this->request)
        );
    }

    public function testSoapFault()
    {
        $this->mockedTravoluteService->method('search')
            ->with(new SearchQueryParameters($this->queryData), 0)
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $controller = new OfferController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse(['error' => 'Error message'], 500),
            $controller->get($this->request)
        );
    }
}
