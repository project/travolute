<?php


namespace Drupal\travolute\tests\Unit;


use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Drupal\travolute\Controller\AccommodationsAutoSuggestController;
use Drupal\travolute\Model\AutoSuggestResponse;
use Drupal\travolute\ValueObject\AutoSuggestParameters;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationsAutoSuggestControllerTest
 *
 * @group travolute-unit
 */

class AccommodationsAutoSuggestControllerTest extends UnitTestCase
{
    private $mockedTravoluteService;
    private $response;
    private $queryData;
    private $request;
    private $autoSuggestResponse;
    private $filters;

    protected function setUp()
    {
        parent::setUp();
        /** @var TravoluteServiceInterface|MockObject $mockedTravoluteService */
        $this->mockedTravoluteService = $this->getMockBuilder(TravoluteService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->response = (object) array(
            "items" => [],
            "responseCount" => 0
        );

        $this->queryData = [
            'query' => 'riu',
        ];

        $this->request = new Request($this->queryData);

        $this->autoSuggestResponse = new AutoSuggestResponse($this->response);

        $this->filters = (new AutoSuggestParameters($this->queryData))->getQuery();
    }

    /**
     * Check if structure of autosuggest response meets expectations
     */
    public function testGetResponse()
    {
        $this->mockedTravoluteService->method('autoSuggest')
            ->with($this->filters)
            ->willReturn($this->autoSuggestResponse);

        $controller = new AccommodationsAutoSuggestController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse((new AutoSuggestResponse($this->response))->jsonSerialize()),
            $controller->get($this->request)
        );
    }

    public function testSoapFault()
    {
        $this->mockedTravoluteService->method('autoSuggest')
            ->with($this->filters)
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $controller = new AccommodationsAutoSuggestController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse(['error' => 'Error message'], 500),
            $controller->get($this->request)
        );
    }
}
