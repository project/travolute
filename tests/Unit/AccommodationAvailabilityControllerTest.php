<?php


namespace Drupal\travolute\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\travolute\Service\TravoluteService;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Drupal\travolute\Controller\AccommodationAvailabilityController;
use Drupal\travolute\Controller\AccommodationController;
use Drupal\travolute\Model\AccommodationDetailResponse;
use Drupal\travolute\Model\AutoSuggestResponse;
use Drupal\travolute\Model\PriceAvailabilityResponse;
use Drupal\travolute\ValueObject\AccommodationAvailabilityParameters;
use Drupal\travolute\ValueObject\AutoSuggestParameters;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationAvailabilityControllerTest
 *
 * @group travolute-unit
 */
class AccommodationAvailabilityControllerTest extends UnitTestCase
{
    private $mockedTravoluteService;
    private $response;
    private $queryData;
    private $request;
    private $availabilityResponse;
    private $filterKey;
    private $filterCustomers;

    protected function setUp()
    {
        parent::setUp();
        /** @var TravoluteServiceInterface|MockObject $mockedTravoluteService */
        $this->mockedTravoluteService = $this->getMockBuilder(TravoluteService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->response = (object) array(
            "bookingCode" => "test",
            "priceAvailabilityModel" => (object) array(
                "priceDetails" => array( (object) array(
                    "description" => "test",
                    "amount" => "test",
                    "type" => "test",
                    "empty" => "test",
                    "price" => "test",
                )),
                "available" => "test",
                "fromFlightModel" => (object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                ),
                "toFlightModel" => (object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                ),
                "toFlightModelList" => array((object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                )),
                "fromFlightModelList" => array((object) array(
                    "id" => 'test',
                    "flight_code" => 'test',
                    "airline_code" => 'test',
                    "airline" => 'test',
                    "flight_number" => 'test',
                    "departure_iata_code" => 'test',
                    "departure_date" => 'test',
                    "departure_time" => 'test',
                    "arrival_iata_code" => 'test',
                    "arrival_date" => 'test',
                    "arrival_time" => 'test',
                    "class_code" => 'test',
                    "class" => 'test',
                )),
                "total_price" => 'test',
                "errata" => 'test',
                "error_response" => 'test',
            ),

        );

        $this->queryData = [
            'key' => 'key',
            'customers' => array(),
        ];

        $this->request = new Request([], [], [], [], [], [], json_encode($this->queryData));

        $this->availabilityResponse = new PriceAvailabilityResponse($this->response);

        $this->filterKey = (new AccommodationAvailabilityParameters($this->queryData))->getKey();

        $this->filterCustomers = (new AccommodationAvailabilityParameters($this->queryData))->getCustomers();
    }

    /**
     * Check if structure of accommodation availability check response meets expectations
     */
    public function testGetResponse()
    {
        $this->mockedTravoluteService->method('availability')
            ->with($this->filterKey, $this->filterCustomers)
            ->willReturn($this->availabilityResponse);

        $controller = new AccommodationAvailabilityController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse((new PriceAvailabilityResponse($this->response))->jsonSerialize()),
            $controller->get($this->request)
        );
    }

    public function testSoapFault()
    {
        $this->mockedTravoluteService->method('availability')
            ->with($this->filterKey, $this->filterCustomers)
            ->willThrowException(new \SoapFault('SOAP-ENV:Server', 'Error message'));

        $controller = new AccommodationAvailabilityController($this->mockedTravoluteService);
        $this->assertEquals(
            new JsonResponse(['error' => 'Error message'], 500),
            $controller->get($this->request)
        );
    }
}
