<?php

namespace Drupal\travolute\tests\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\travolute\ValueObject\Booking;

/**
 * Class BookingValueObjectTest
 *
 * @group travolute-unit
 */
class BookingValueObjectTest extends UnitTestCase
{

  /**
   * @param string $getter
   * @param $value
   * @param \stdClass $constructorData
   *
   * @dataProvider dataProviderGetterValues
   */
  public function testGetterWillFetchDataSetInConstructor(string $getter, $value, \stdClass $constructorData): void
  {
    $booking = new Booking($constructorData);
    $this->assertEquals($value, $booking->{$getter}());
  }

  public function dataProviderGetterValues(): array
  {
    $priceModel = (object)[
      'int' => 123,
      'float' => 1.2,
      'string' => 'price',
      'array' => [
        'int' => 1235,
        'float' => 1.25,
        'string' => 'price',
      ],
      'object' => (object) [
        'int' => 1234,
        'float' => 1.23,
        'string' => 'price',
      ],
    ];
    $priceAvailabilityModel = (object)[
      'int' => 1234,
      'float' => 1.24,
      'string' => 'availability',
      'array' => [
        'int' => 12355,
        'float' => 1.255,
        'string' => 'availability',
      ],
      'object' => (object) [
        'int' => 12346,
        'float' => 1.236,
        'string' => 'availability',
      ],
    ];
    $accommodationModel = (object)[
      'int' => 12345,
      'float' => 1.246,
      'string' => 'accommodation',
      'array' => [
        'int' => 123557,
        'float' => 1.2559,
        'string' => 'accommodation',
      ],
      'object' => (object) [
        'int' => 123460,
        'float' => 1.2360,
        'string' => 'accommodation',
      ],
    ];
    return [
      'getId' => [
        '$getter' => 'getId',
        '$value' => 123,
        '$constructorData' => (object)[
          'id' => 123,
        ],
      ],
      'getCode' => [
        '$getter' => 'getCode',
        '$value' => 'somePr3ttyH@sh',
        '$constructorData' => (object)[
          'code' => 'somePr3ttyH@sh',
        ],
      ],
      'getPriceModel' => [
        '$getter' => 'getPriceModel',
        '$value' => $priceModel,
        '$constructorData' => (object)[
          'priceModel' => $priceModel,
        ],
      ],
      'getPriceAvailabilityModel' => [
        '$getter' => 'getPriceAvailabilityModel',
        '$value' => $priceAvailabilityModel,
        '$constructorData' => (object)[
          'priceAvailabilityModel' => $priceAvailabilityModel,
        ],
      ],
      'getTotalPrice' => [
        '$getter' => 'getTotalPrice',
        '$value' => $priceModel,
        '$constructorData' => (object)[
          'total_price' => $priceModel,
        ],
      ],
      'getPriceDetails' => [
        '$getter' => 'getPriceDetails',
        '$value' => $priceModel,
        '$constructorData' => (object)[
          'priceDetails' => $priceModel,
        ],
      ],
      'getAccommodationModel' => [
        '$getter' => 'getAccommodationModel',
        '$value' => $accommodationModel,
        '$constructorData' => (object)[
          'accommodationModel' => $accommodationModel,
        ],
      ],
    ];
  }

  public function testJsonSerialize(): void {
    $booking = new Booking(
      (object) [
      'id' => 12346,
      'externalCode' => 'aN1c3ExternalC0d3',
    ]);
    $result = $booking->jsonSerialize();
    $this->assertArrayEquals([
      'id' => 12346,
      'externalCode' => 'aN1c3ExternalC0d3',
    ], $result);
  }
}
