<?php


namespace Drupal\travolute\ValueObject;


final class AccommodationAvailabilityParameters
{
    private $key;
    private $customers;

    public function __construct($postData)
    {
        $this->key = $postData['key'];
        $this->customers = $postData['customers'];
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getCustomers()
    {
        return $this->customers;
    }

}
