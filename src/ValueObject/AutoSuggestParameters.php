<?php


namespace Drupal\travolute\ValueObject;


final class AutoSuggestParameters
{
    private $query;

    public function __construct($queryData)
    {
        $this->query = '';
        if (isset($queryData['query']) && strlen($queryData['query']) > 0) {
            $this->query = str_replace('-', ' ', $queryData['query']);
        };
    }

   public function getQuery() {
        return $this->query;
   }
}
