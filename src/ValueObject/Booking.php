<?php


namespace Drupal\travolute\ValueObject;
use \JsonSerializable;

class Booking implements JsonSerializable {

  protected $id;
  protected $code;
  protected $externalCode;
  protected $agent;
  protected $departureDate;
  protected $accommodationName;
  protected $travelAgentName;
  protected $created_at;
  protected $updated_at;
  protected $accommodationId;
  protected $numberOfDays;
  protected $roomTypeId;
  protected $boardTypeId;
  protected $occupancy;
  protected $departureAirportId;
  protected $customers;
  protected $priceModel;
  protected $priceAvailabilityModel;
  protected $accommodationModel;
  protected $priceDetails;
  protected $totalPrice;


  public function __construct($newBooking) {
    if (property_exists($newBooking, 'id')) {
      $this->id = $newBooking->id;
    }
    if (property_exists($newBooking, 'externalCode')) {
      $this->externalCode = $newBooking->externalCode;
    }
    if (property_exists($newBooking, 'agent')) {
      $this->agent = $newBooking->agent;
    }
    if (property_exists($newBooking, 'departureDate')) {
      $this->departureDate = $newBooking->departureDate;
    }
    if (property_exists($newBooking, 'accommodationName')) {
      $this->accommodationName = $newBooking->accommodationName;
    }
    if (property_exists($newBooking, 'travelAgentName')) {
      $this->travelAgentName = $newBooking->travelAgentName;
    }
    if (property_exists($newBooking, 'created_at')) {
      $this->created_at = $newBooking->created_at;
    }
    if (property_exists($newBooking, 'updated_at')) {
      $this->updated_at = $newBooking->updated_at;
    }
    if (property_exists($newBooking, 'code')) {
      $this->code = $newBooking->code;
    }
    if (property_exists($newBooking, 'accommodationId')) {
      $this->accommodationId = $newBooking->accommodationId;
    }
    if (property_exists($newBooking, 'numberOfDays')) {
      $this->numberOfDays = $newBooking->numberOfDays;
    }
    if (property_exists($newBooking, 'roomTypeId')) {
      $this->roomTypeId = $newBooking->roomTypeId;
    }
    if (property_exists($newBooking, 'boardTypeId')) {
      $this->boardTypeId = $newBooking->boardTypeId;
    }
    if (property_exists($newBooking, 'occupancy')) {
      $this->occupancy = $newBooking->occupancy;
    }
    if (property_exists($newBooking, 'departureAirportId')) {
      $this->departureAirportId = $newBooking->departureAirportId;
    }
    if (property_exists($newBooking, 'customers')) {
      $this->customers = $newBooking->customers;
    }
    if (property_exists($newBooking, 'priceModel')) {
      $this->priceModel = $newBooking->priceModel;
    }
    if (property_exists($newBooking, 'priceAvailabilityModel')) {
      $this->priceAvailabilityModel = $newBooking->priceAvailabilityModel;
    }
    if (property_exists($newBooking, 'accommodationModel')) {
      $this->accommodationModel = $newBooking->accommodationModel;
    }
    if (property_exists($newBooking, 'priceDetails')) {
      $this->priceDetails = $newBooking->priceDetails;
    }
    if (property_exists($newBooking, 'total_price')) {
      $this->totalPrice = $newBooking->total_price;
    }
  }

  public function getId() {
    return $this->id;
  }

  public function getCode() {
    return $this->code;
  }

  public function getPriceModel() {
    return $this->priceModel;
  }

  public function getPriceAvailabilityModel() {
    return $this->priceAvailabilityModel;
  }

  public function getTotalPrice() {
    return $this->totalPrice;
  }
  public function getPriceDetails() {
    return $this->priceDetails;
  }

  public function getAccommodationModel() {
    return $this->accommodationModel;
  }

  public function jsonSerialize(): array {
    return [
      'id' => $this->id,
      'externalCode' => $this->externalCode,
    ];
  }

}
