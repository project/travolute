<?php

namespace Drupal\travolute\ValueObject;

final class FilterQueryParameters
{
    private $params;
    private $items;

    public function __construct($queryData)
    {
        $newItems = [];
        if (isset($queryData['items']) && strlen($queryData['items']) > 0) {
            $newItems = explode(',', $queryData['items']);
        };

        $newParams = [];
        if (isset($queryData['country'])) {
            $newParams['country'] = explode(',', $queryData['country']);
        };

        if (isset($queryData['region'])) {
            $newParams['region'] = explode(',', $queryData['region']);
        };

        if (isset($queryData['airport'])) {
            $newParams['airport'] = explode(',', $queryData['airport']);
        };

        if (isset($queryData['city'])) {
            $newParams['city'] = explode(',', $queryData['city']);
        };

        $this->items = $newItems;
        $this->params = $newParams;
    }

    public function toArray() {
        return array($this->items, $this->params);
    }
}
