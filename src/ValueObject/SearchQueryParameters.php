<?php

namespace Drupal\travolute\ValueObject;

final class SearchQueryParameters
{
    private $params;

    public function __construct($params)
    {
        if (isset($params['country'])) {
            $params['country'] = explode(',', $params['country']);
        }
        if (isset($params['airport'])) {
            $params['airport'] = explode(',', $params['airport']);
        }
        if (isset($params['duration'])) {
            $params['duration'] = explode(',', $params['duration']);
        }
        if (isset($params['boardtype'])) {
            $params['boardtype'] = explode(',', $params['boardtype']);
        }
        if (isset($params['city'])) {
            $params['city'] = explode(',', $params['city']);
        }
        if (isset($params['region'])) {
            $params['region'] = explode(',', $params['region']);
        }
        if (isset($params['page'])) {
            unset($params['page']);
        }

        $this->params = $params;
    }

    public function toArray() {
        return $this->params;
    }
}
