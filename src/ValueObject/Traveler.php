<?php

namespace Drupal\travolute\ValueObject;

final class Traveler
{
    private $firstname;
    private $lastname;
    private $gender;
    private $day;
    private $month;
    private $year;

    /**
     * Traveler constructor.
     * @param $firstname
     * @param $lastname
     * @param $gender
     * @param $day
     * @param $month
     * @param $year
     */
    public function __construct($firstname, $lastname, $gender, $day, $month, $year)
    {
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->gender = $gender;
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @return mixed
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * @return mixed
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }
}
