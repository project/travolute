<?php

namespace Drupal\travolute\ValueObject;


use Carbon\Carbon;

final class AccommodationParameters
{
    private $departureDate;
    private $returnDate;
    private $departureOffset;
    private $query;
    private $minimumNumberOfStars;
    private $durations;
    private $boardTypes;
    private $occupancy;
    private $offset;
    private $limit;
    private $airport;
    private $adults;
    private $children;

  /**
   * AccommodationParameters constructor.
   *
   * @param $postData
   * @throws \Exception
   */
    public function __construct($postData) {
        $this->departureDate = (!empty($postData['departure_date']) ? new Carbon($postData['departure_date']) : null);
        $this->returnDate = (!empty($postData['return_date']) ? new Carbon($postData['return_date']) : null);
        $this->departureOffset = (!empty($postData['departure_offset']) ? $postData['departure_offset'] : null);
        $this->query = (!empty($postData['query']) ? $postData['query'] : null);
        $this->minimumNumberOfStars = (!empty($postData['stars']) ? $postData['stars'] : null);
        $this->durations = (!empty($postData['durations']) ? $postData['durations'] : null);
        $this->boardTypes = (!empty($postData['boardType']) ? $postData['boardType'] : null);
        $this->occupancy = (!empty($postData['occupancy']) ? $postData['occupancy'] : 2);
        $this->offset = (!empty($postData['offset']) ? $postData['offset'] : 0);
        $this->limit = (!empty($postData['limit']) ? $postData['limit'] : 20);
        $this->airport = (!empty($postData['airport']) ? $postData['airport'] : null);
        $this->adults = (!empty($postData['pax']['adults']) ? $this->convertToTravelers($postData['pax']['adults']) : null);
        $this->children = (!empty($postData['pax']['children']) ? $this->convertToTravelers($postData['pax']['children']) : null);
    }

    public function setLimit (int $limit): void {
      $this->limit = $limit;
    }

    /**
     * @return Carbon
     */
    public function getDepartureDate(): Carbon
    {
        return $this->departureDate;
    }

    /**
     * @return Carbon
     */
    public function getReturnDate(): Carbon
    {
        return $this->returnDate;
    }

    /**
     * @return int
     */
    public function getDepartureOffset(): int
    {
        return $this->departureOffset;
    }

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return mixed
     */
    public function getMinimumNumberOfStars()
    {
        return $this->minimumNumberOfStars;
    }

    /**
     * @return mixed
     */
    public function getDurations()
    {
        return $this->durations;
    }

    /**
     * @return array
     */
    public function getBoardTypes(): array
    {
        return $this->boardTypes;
    }

    /**
     * @return int
     */
    public function getOccupancy(): int
    {
        return $this->occupancy;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @return array|null
     */
    public function getAirport(): ?array
    {
        return $this->airport;
    }

    /**
     * @return array|null
     */
    public function getAdults(): ?array
    {
        return $this->adults;
    }

    /**
     * @return array|null
     */
    public function getChildren(): ?array
    {
        return $this->children;
    }

    /**
     * Converts the travelers input query to value object
     *
     * @param array $paxArray
     * @return array
     */
    private function convertToTravelers(array $paxArray): array {
        $adults = [];
        foreach($paxArray as $pax) {
            $dateofbirth = new Carbon($pax["dateofbirth"]);
            $adults[] = new Traveler(
                $pax['firstName'],
                $pax['lastName'],
                $pax['gender'] === 'male' ? 'M' : 'V',
                $dateofbirth->day,
                $dateofbirth->month,
                $dateofbirth->year,
            );
        }

        return $adults;
    }

    public function toArray()
    {
        $output = [
            'occupancy' => $this->getOccupancy(),
            'offset' => $this->getOffset(),
            'limit' => $this->getLimit(),
        ];

        if ($this->departureDate instanceof Carbon) {
            $output['departure_date'] = $this->getDepartureDate()->format('Y-m-d');
        }

        if ($this->returnDate instanceof Carbon) {
            $output['return_date'] = $this->getReturnDate()->format('Y-m-d');
        }

        if ($this->departureOffset !== null) {
            $output['departure_offset'] = $this->getDepartureOffset();
        }

        if (\is_string($this->query)) {
            $output['query'] = $this->getQuery();
        }

        if (\is_int($this->minimumNumberOfStars)) {
            $output['stars'] = $this->getMinimumNumberOfStars();
        }

        if (\is_array($this->durations)) {
            $output['duration'] = $this->getDurations();
        }

        if (\is_array($this->airport)) {
            $output['airport'] = $this->getAirport();
        }

        if (\is_array($this->boardTypes)) {
            $output['boardtype'] = $this->getBoardTypes();
        }

        if (\is_array($this->adults)) {
            $output['adults'] = $this->getAdults();
        }

        if (\is_array($this->children)) {
            $output['children'] = $this->getChildren();
        }

        return $output;
    }
}
