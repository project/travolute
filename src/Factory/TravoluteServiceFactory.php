<?php

namespace Drupal\travolute\Factory;

use Drupal;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\travolute\Service\TravoluteService;
use SoapClient;

class TravoluteServiceFactory
{
    public static function createTravoluteService(SoapClient $soapClient, CacheBackendInterface $cache)
    {
        $config = Drupal::config('travolute.schema');
        $apiKey = $config->get('api_key');
        $apiSecret = $config->get('api_secret');

        if (function_exists('getenv') && !empty(getenv('TRAVOLUTE_API_KEY')) ) {
            $apiKey = getenv('TRAVOLUTE_API_KEY');
            $apiSecret = getenv('TRAVOLUTE_API_SECRET');
        }

        return new TravoluteService(
            $apiKey,
            $apiSecret,
            $soapClient,
            $cache
        );
    }
}
