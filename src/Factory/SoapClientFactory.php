<?php

namespace Drupal\travolute\Factory;

use Drupal;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\travolute\Service\TravoluteService;
use \SoapClient;

class SoapClientFactory
{
    public static function createSoapClient()
    {
        $wsdl = "http://api.travolute.netants.nl/soap?wsdl";
        if (function_exists('getenv') && !empty(getenv('TRAVOLUTE_WSDL')) ) {
            $wsdl = getenv('TRAVOLUTE_WSDL');
        }

        return new SoapClient(
            $wsdl,
            [
                'cache_wsdl' => WSDL_CACHE_NONE,
                'compression' => SOAP_COMPRESSION_ACCEPT,
                'trace' => 1
            ]
        );
    }
}
