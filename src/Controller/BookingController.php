<?php
declare(strict_types=1);

namespace Drupal\travolute\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BookingController extends ControllerBase
{

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return JsonResponse
   * @url api/v1/booking
   * @throws \JsonException
   */
  public function booking(Request $request): JsonResponse
  {
    $requestData = json_decode(
      $request->getContent(),
      false,
      512
    );
    $bookingCode = $requestData->bookingCode;
    $customers = $requestData->customers;
    try {
      $result = $this->travoluteService->booking($bookingCode, $customers);
    } catch (\SoapFault $e) {
      return new JsonResponse(['error' => $e->getMessage()], 500);
    }
    return new JsonResponse($result);
  }

  /**
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return JsonResponse
   * @url api/v1/booking
   * @throws \JsonException
   */
  public function preBooking(Request $request): JsonResponse
  {
    $requestData = json_decode(
      $request->getContent(),
      false,
      512
    );
    try {
      $result = $this->travoluteService->preBooking($requestData->bookingCode);
    } catch (\SoapFault $e) {
      return new JsonResponse(['error' => $e->getMessage()], 500);
    }
    return new JsonResponse($result);
  }
}
