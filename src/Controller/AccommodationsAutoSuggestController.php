<?php

declare(strict_types=1);

namespace Drupal\travolute\Controller;

use Drupal\travolute\ValueObject\AutoSuggestParameters;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationController
 * @package Drupal\netants_phpunit_example\Controller
 */
class AccommodationsAutoSuggestController extends ControllerBase
{

    /**
     * Accommodations from the api
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @url api/v1/accommodations/*id
     */
    public function get(Request $request): JsonResponse
    {
        $query = (new AutoSuggestParameters($request->query->all()))->getQuery();

        try {
            $response = $this->travoluteService->autoSuggest($query);

            /* if the filters is not found the API throws a SoapFault (what if something else caused a soapfailt?) */
        } catch (\SoapFault $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse($response->jsonSerialize());
    }
}
