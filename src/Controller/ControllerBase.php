<?php


namespace Drupal\travolute\Controller;


use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\travolute\Service\TravoluteServiceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class ControllerBase extends \Drupal\Core\Controller\ControllerBase
{
    use StringTranslationTrait;

    /**
     * @var TravoluteServiceInterface $travoluteService
     */
    protected $travoluteService;


    public function __construct(TravoluteServiceInterface $travoluteService)
    {
        $this->travoluteService = $travoluteService;
    }

    public static function create(ContainerInterface $container) {
        return new static($container->get('travolute.service'));
    }
}
