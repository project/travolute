<?php

declare(strict_types=1);

namespace Drupal\travolute\Controller;

use Drupal\travolute\ValueObject\SearchQueryParameters;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationController
 * @package Drupal\netants_phpunit_example\Controller
 */
class OfferController extends ControllerBase
{

    /**
     * Fetches filters from the travolute API.
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @url api/v1/filters/*id
     */
    public function get(Request $request): JsonResponse
    {
        $params = new SearchQueryParameters($request->query->all());

        $page = $request->query->get('page');
        if (is_null($page)) {
            $page = 0;
        }

        try {
            $response = $this->travoluteService->search($params, $page);

            /* if the filters is not found the API throws a SoapFault (what if something else caused a soapfailt?) */
        } catch (\SoapFault $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse($response->jsonSerialize());
    }
}
