<?php
declare(strict_types=1);

namespace Drupal\travolute\Controller;

use Drupal\travolute\ValueObject\AccommodationAvailabilityParameters;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationAvailabilityController
 * @package Drupal\netants_phpunit_example\Controller
 */
class AccommodationAvailabilityController extends ControllerBase
{

    /**
     * Fetches a accommodation availability from the travolute API.
     * If the accommodation availability key is not found it will throw a 404 not found
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     *
     * @return JsonResponse
     * @url api/v1/accommodations_availability/*id
     */
    public function get(Request $request): JsonResponse
    {
        $filters = new AccommodationAvailabilityParameters(json_decode($request->getContent(), true));

        try {
            $response = $this->travoluteService->availability($filters->getKey(), $filters->getCustomers());

            /* if the accommodation is not found the API throws a SoapFault (what if something else caused a soapfailt?) */
        } catch (\SoapFault $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse( $response );
    }
}
