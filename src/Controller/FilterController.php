<?php

declare(strict_types=1);

namespace Drupal\travolute\Controller;

use Drupal\travolute\ValueObject\FilterQueryParameters;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationController
 * @package Drupal\netants_phpunit_example\Controller
 */
class FilterController extends ControllerBase
{
    /**
     * Fetches filters from the travolute API.
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @url api/v1/filters/*id
     */
    public function get(Request $request): JsonResponse
    {
        list($items, $params) = (new FilterQueryParameters($request->query->all()))->toArray();

        try {
            $response = $this->travoluteService->getFilters($items, $params);

            /* if the filters is not found the API throws a SoapFault (what if something else caused a soapfailt?) */
        } catch (\SoapFault $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse($response->jsonSerialize());
    }
}
