<?php
declare(strict_types=1);

namespace Drupal\travolute\Controller;

use Carbon\Carbon;
use Drupal\travolute\ValueObject\AccommodationParameters;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AccommodationController
 * @package Drupal\netants_phpunit_example\Controller
 */
class AccommodationController extends ControllerBase
{

  /**
   * Fetches a accommodation detail from the travolute API.
   * If the ID is not conform specs it will throw a 500 bad request
   * If the accommodation is not found it will throw a 404 not found
   *
   * @param int $giataId
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return JsonResponse
   * @throws \Exception
   * @url api/v1/accommodations/*id
   */
    public function get(int $giataId, Request $request): JsonResponse
    {
        $params = new AccommodationParameters(json_decode($request->getContent(), true));

        try {
            $response = $this->travoluteService->detail($giataId, $params);

            /* if the accommodation is not found the API throws a SoapFault (what if something else caused a soapfailt?) */
        } catch (\SoapFault $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse( $response );
    }

  /**
   * Fetches a accommodation detail from the travolute API.
   * Retrieve the prices grouped based on date and duration
   * If the ID is not conform specs it will throw a 500 bad request
   * If the accommodation is not found it will throw a 404 not found
   *
   * @param int $giataId
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return JsonResponse
   * @throws \Exception
   * @url api/v1/accommodations/*id
   */
    public function getAccommodationsGroupedByDateAndDuration(int $giataId, Request $request): JsonResponse
    {
        $decodedRequest = json_decode($request->getContent(), true);
        $params = new AccommodationParameters($decodedRequest);
        if (
          array_key_exists('limit', $decodedRequest)
          && !$decodedRequest['limit']
        ) {
            $params->setLimit(100);
        }

        try {
            $response = $this->travoluteService->detailWithGroupedPricesBasedOnDateAndDuration(
              $giataId,
              $params
            );
        /* if the accommodation is not found the API throws a SoapFault (what if something else caused a soapfailt?) */
        } catch (\SoapFault $e) {
            return new JsonResponse(['error' => $e->getMessage()], 500);
        }

        return new JsonResponse( $response );
    }
}
