<?php
namespace Drupal\travolute\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class TravoluteSettingsForm extends ConfigFormBase {

    /**
     * Config settings.
     *
     * @var string
     */
    const SETTINGS = 'travolute.schema';

    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'travolute_admin_settings';
    }

    /**
     * {@inheritdoc}
     */
    protected function getEditableConfigNames() {
        return [
            static::SETTINGS,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $config = $this->config(static::SETTINGS);
        $disabled = false;
        $apiKey = $config->get('api_key');
        $apiSecret = $config->get('api_secret');

        if (function_exists('getenv') && !empty(getenv('TRAVOLUTE_API_KEY')) ) {
            $disabled = true;
            $apiKey = getenv('TRAVOLUTE_API_KEY');
            $apiSecret = '********';
        }

        $form['api_key'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Api key'),
            '#default_value' => $apiKey,
            '#disabled' => $disabled,
        ];

        $form['api_secret'] = [
            '#type' => 'textfield',
            '#title' => $this->t('Api secret'),
            '#default_value' => $apiSecret,
            '#disabled' => $disabled,
        ];

        return parent::buildForm($form, $form_state);
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {
        // Disable submission when using .env
        if (function_exists('getenv') && !empty(getenv('TRAVOLUTE_API_KEY')) ) {
            return;
        }

        // Retrieve the configuration.
        $this->configFactory->getEditable(static::SETTINGS)
            // Set the submitted configuration setting.
            ->set('api_key', $form_state->getValue('api_key'))
            // You can set multiple configurations at once by making
            // multiple calls to set().
            ->set('api_secret', $form_state->getValue('api_secret'))
            ->save();

        parent::submitForm($form, $form_state);
    }

}
