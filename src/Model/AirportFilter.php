<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class AirportFilter implements JsonSerializable
{
  /**
   * The key of the filter
   *
   * @var string
   */
  private $key;

  /**
   * The value of the filter
   *
   * @var string
   */
  private $value;

  /**
   * The lat of the filter
   *
   * @var float
   */
  private $lat;

  /**
   * The long of the filter
   *
   * @var float
   */
  private $long;

  /**
   * Filter constructor.
   *
   * @param string $key
   * @param string $value
   * @param float $lat
   * @param float $long
   */
  public function __construct(string $key, string $value, float $lat, float $long)
  {
    $this->key = $key;
    $this->value = $value;
    $this->lat = $lat;
    $this->long = $long;
  }

  /**
   * @return string
   */
  public function getKey(): string
  {
    return $this->key;
  }

  /**
   * @return string
   */
  public function getValue(): string
  {
    return $this->value;
  }

  /**
   * @return array
   */
  public function jsonSerialize(): array
  {
      return [
        'key' => $this->key,
        'value' => $this->value,
        'lat' => $this->lat,
        'lng' => $this->long,
      ];
  }
}
