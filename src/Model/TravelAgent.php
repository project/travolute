<?php

namespace Drupal\travolute\Model;


class TravelAgent
{
    /**
     * The unique key of the TravelAgent Model
     *
     * @var string
     */
    private $id;

    /**
     * The code of the TravelAgent Model
     *
     * @var string
     */
    private $code;

    /**
     * The name of the TravelAgent Model
     *
     * @var string
     */
    private $name;

    /**
     * TravelAgent constructor.
     *
     * @param $model
     */
    public function __construct(
      $model
    )
    {
        $this->id = $model->id;
        $this->code = $model->code;
        $this->name = $model->name;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
          'id' => $this->id,
          'code' => $this->code,
          'name' => $this->name
        ];
    }
}
