<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class Filter implements JsonSerializable
{
    /**
     * The key of the filter
     *
     * @var string
     */
    private $key;

    /**
     * The value of the filter
     *
     * @var string
     */
    private $value;

    /**
     * Filter constructor.
     *
     * @param string $key
     * @param string $value
     */
    public function __construct(string $key, string $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'key' => $this->key,
            'value' =>$this->value,
        ];
    }
}
