<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

class Country implements \JsonSerializable
{
    /**
     * The travolute id of the country
     *
     * @var int
     */
    private $id;

    /**
     * @var string The name of the country
     */
    private $name;

    /**
     * Country constructor.
     *
     * @param int $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name
        ];
    }
}
