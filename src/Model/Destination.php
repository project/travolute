<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

/**
 * Class Destination
 * @package Drupal\travolute\Model
 */
class Destination implements \JsonSerializable
{
    /**
     * The travolute id of the destination (place)
     *
     * @var int
     */
    private $id;

    /**
     * The name of the destination
     *
     * @var string
     */
    private $name;

    /**
     * The region model of this destination
     *
     * @var Region
     */
    private $region;

    /**
     * The country model of this destination
     *
     * @var Country
     */
    private $country;

    /**
     * Destination constructor.
     *
     * @param int $id
     * @param string $name
     * @param Region $region
     * @param Country $country
     */
    public function __construct(int $id, string $name, ?Region $region, Country $country)
    {
        $this->id = $id;
        $this->name = $name;
        $this->region = $region;
        $this->country = $country;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Region
     */
    public function getRegion(): Region
    {
        return $this->region;
    }

    /**
     * @return Country
     */
    public function getCountry(): Country
    {
        return $this->country;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'country' => $this->country,
            'name' => $this->name,
            'region' => $this->region
        ];
    }
}
