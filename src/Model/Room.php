<?php

namespace Drupal\travolute\Model;


class Room
{
    /**
     * The unique key of the Room Model
     *
     * @var string
     */
    private $id;

    /**
     * The code of the Room Model
     *
     * @var string
     */
    private $unit_type;

    /**
     * The name of the Room Model
     *
     * @var string
     */
    private $unit_id;

    /**
     * TravelAgent constructor.
     *
     * @param $model
     */
    public function __construct(
      $model
    )
    {
        $this->id = $model->id;
        $this->unit_type = $model->unit_type;
        $this->unit_id = $model->unit_id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUnitType(): string
    {
        return $this->unit_type;
    }

    /**
     * @return string
     */
    public function getUnitId(): string
    {
        return $this->unit_id;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
          'id' => $this->id,
          'unit_type' => $this->unit_type,
          'unit_id' => $this->unit_id
        ];
    }
}
