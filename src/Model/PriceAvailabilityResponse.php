<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

class PriceAvailabilityResponse implements \JsonSerializable
{
    private $bookingCode;
    private $available;
    /**
     * @var Flight
     */
    private $toFlight;

    /** @var Flight[] */
    public $toFlightModelList = [];
    /**
     * @var Flight
     */
    private $fromFlight;
    /** @var Flight[] */
    public $fromFlightModelList = [];
    /**
     * @var array
     */
    private $priceDetails;
    private $totalPrice;
    private $errata;
    private $errorResponse;

    /**
     * AccommodationDetailResponse constructor.
     *
     * Receives the XML from the Travolute API and converts it to Accommodation response model
     *
     * @param \stdClass $response
     */
    public function __construct(\stdClass $response)
    {
        if (!isset($response->priceAvailabilityModel)) {
            throw new \InvalidArgumentException('Invalid price availability response');
        }

        $item = $response->priceAvailabilityModel;

        $priceDetails = [];
        foreach ($item->priceDetails as $priceDetail) {
            $priceDetails[] = new PriceDetail(
                $priceDetail->description,
                $priceDetail->amount,
                $priceDetail->type,
                $priceDetail->empty,
                $priceDetail->price
            );
        }
        $this->bookingCode = $response->bookingCode;
        $this->available = $item->available;
        $this->toFlight = new Flight(
            $item->toFlightModel->id,
            $item->toFlightModel->flight_code,
            $item->toFlightModel->airline_code,
            $item->toFlightModel->airline,
            $item->toFlightModel->flight_number,
            $item->toFlightModel->departure_iata_code,
            $item->toFlightModel->departure_date,
            $item->toFlightModel->departure_time,
            $item->toFlightModel->arrival_iata_code,
            $item->toFlightModel->arrival_date,
            $item->toFlightModel->arrival_time,
            $item->toFlightModel->class_code,
            $item->toFlightModel->class
        );
        $this->fromFlight = new Flight(
            $item->fromFlightModel->id,
            $item->fromFlightModel->flight_code,
            $item->fromFlightModel->airline_code,
            $item->fromFlightModel->airline,
            $item->fromFlightModel->flight_number,
            $item->fromFlightModel->departure_iata_code,
            $item->fromFlightModel->departure_date,
            $item->fromFlightModel->departure_time,
            $item->fromFlightModel->arrival_iata_code,
            $item->fromFlightModel->arrival_date,
            $item->fromFlightModel->arrival_time,
            $item->fromFlightModel->class_code,
            $item->fromFlightModel->class
        );
        foreach ($item->toFlightModelList as $flightModel) {
            $this->toFlightModelList[] = new Flight(
                $flightModel->id,
                $flightModel->flight_code,
                $flightModel->airline_code,
                $flightModel->airline,
                $flightModel->flight_number,
                $flightModel->departure_iata_code,
                $flightModel->departure_date,
                $flightModel->departure_time,
                $flightModel->arrival_iata_code,
                $flightModel->arrival_date,
                $flightModel->arrival_time,
                $flightModel->class_code,
                $flightModel->class
            );
        }
        foreach ($item->fromFlightModelList as $flightModel) {
            $this->fromFlightModelList[] = new Flight(
                $flightModel->id,
                $flightModel->flight_code,
                $flightModel->airline_code,
                $flightModel->airline,
                $flightModel->flight_number,
                $flightModel->departure_iata_code,
                $flightModel->departure_date,
                $flightModel->departure_time,
                $flightModel->arrival_iata_code,
                $flightModel->arrival_date,
                $flightModel->arrival_time,
                $flightModel->class_code,
                $flightModel->class
            );
        }
        $this->priceDetails = $priceDetails;
        $this->totalPrice = $item->total_price;
        $this->errata = $item->errata;
        $this->errorResponse = $item->error_response;

    }

    public function jsonSerialize(): array
    {
        return [
            'bookingCode' => $this->bookingCode,
            'available' => $this->available,
            'toFlight' => $this->toFlight,
            'fromFlight' => $this->fromFlight,
            'toFlightModelList' => $this->toFlightModelList,
            'fromFlightModelList' => $this->fromFlightModelList,
            'priceDetails' => $this->priceDetails,
            'totalPrice' => $this->totalPrice,
            'errata' => $this->errata,
            'errorResponse' => $this->errorResponse
        ];
    }
}
