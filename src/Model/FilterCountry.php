<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class FilterCountry implements JsonSerializable
{
    /**
     * The key of the filter
     *
     * @var string
     */
    private $key;

    /**
     * The value of the filter
     *
     * @var string
     */
    private $value;

    /**
     * The country_id of the filter
     *
     * @var string
     */
    private $country_id;

    /**
     * The country of the filter
     *
     * @var string
     */
    private $country;

    /**
     * Filter constructor.
     *
     * @param string $key
     * @param string $value
     * @param string $country_id
     * @param string $country
     */
    public function __construct(
        string $key,
        string $value,
        string $country_id,
        string $country
    ) {
        $this->key = $key;
        $this->value = $value;
        $this->country_id = $country_id;
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function getCountryId(): string
    {
        return $this->country_id;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'key' => $this->key,
            'value' => $this->value,
            'country_id' => $this->country_id,
            'country' => $this->country,
        ];
    }
}
