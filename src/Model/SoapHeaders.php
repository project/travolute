<?php


namespace Drupal\travolute\Model;


final class SoapHeaders
{
    public $auth_token;
    public $auth_token_expire;

    public function __construct($auth_token, $auth_token_expire)
    {
        $this->auth_token = $auth_token;
        $this->auth_token_expire = $auth_token_expire;
    }
}
