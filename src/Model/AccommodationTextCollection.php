<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

class AccommodationTextCollection implements \IteratorAggregate, \JsonSerializable
{
    private $texts;

    public function __construct(AccommodationText ...$texts)
    {
        $this->texts = $texts;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->texts);
    }

    /**
     * Creates a text collection from travolute accommodation response
     *
     * @param array $texts
     * @return AccommodationTextCollection
     */
    public static function createFromTravoluteAccommodationResponse(array $texts): AccommodationTextCollection
    {
        $collection = [];
        foreach ($texts as $text) {
            $collection[] = new AccommodationText($text->textTitle, $text->textPara, $text->textLanguage);
        }

        return new AccommodationTextCollection(...$collection);
    }

    public function jsonSerialize(): array
    {
        return $this->texts;
    }
}
