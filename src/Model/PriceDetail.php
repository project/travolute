<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

class PriceDetail implements \JsonSerializable
{
    private $description;
    private $amount;
    private $type;
    private $empty;
    private $price;

    /**
     * PriceDetail constructor.
     *
     * @param $description
     * @param $amount
     * @param $type
     * @param $empty
     * @param $price
     */
    public function __construct(
        $description,
        $amount,
        $type,
        $empty,
        $price
    ) {
        $this->description = $description;
        $this->amount = $amount;
        $this->type = $type;
        $this->empty = $empty;
        $this->price = $price;
    }

    public function jsonSerialize(): array
    {
        return [
            'description' => $this->description,
            'amount' => $this->amount,
            'type' => $this->type,
            'empty' => $this->empty,
            'price' => $this->price
        ];
    }
}
