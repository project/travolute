<?php


declare(strict_types=1);

namespace Drupal\travolute\Model;

use Carbon\Carbon;
use Drupal\travolute\Model\TravelAgent;
use JsonSerializable;

class AccommodationPrice implements JsonSerializable
{
  /**
   * The unique key of the price model
   *
   * @var string
   */
  private $key;

  /**
   * The unique key from Traffics of the price model
   *
   * @var string
   */
  private $trafficsCode;

  /**
   * The departure date of the price model
   *
   * @var Carbon
   */
  private $departure_date;

  /**
   * The duration of the price model
   *
   * @var int
   */
  private $duration;

  /**
   * The board type as string
   *
   * @var BoardType
   */
  private $boardType;

  /**
   * The occupancy (sum of adults and children)
   *
   * @var int
   */
  private $occupancy;

  /**
   * The price
   *
   * @var float
   */
  private $price;

  /**
   * The travelAgent
   *
   * @var TravelAgent
   */
  private $travelAgents;

  /**
   * The room
   *
   * @var Room
   */
  private $room;

  /**
   * The airport
   *
   * @var Airport
   */
  private $airport;

  /**
   * Price constructor.
   *
   * @param string $key
   * @param string $trafficsCode
   * @param string $departure_date
   * @param int $duration
   * @param BoardType $boardType
   * @param int $occupancy
   * @param float $price
   * @param TravelAgent $travelAgent
   * @param Room $room
   * @param Airport $airport
   */
  public function __construct(
    string $key,
    string $departure_date,
    int $duration,
    BoardType $boardType,
    int $occupancy,
    float $price,
    TravelAgent $travelAgent,
    Room $room,
    Airport $airport,
    string $trafficsCode = null
  ) {
    if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $departure_date)) {
      throw new \InvalidArgumentException('Departure date invalid format');
    }
    $this->key = $key;
    $this->trafficsCode = $trafficsCode;
    $this->departure_date = new Carbon($departure_date);
    $this->duration = $duration;
    $this->boardType = $boardType;
    $this->occupancy = $occupancy;
    $this->price = $price;
    $this->travelAgents = $travelAgent;
    $this->room = $room;
    $this->airport = $airport;
  }

  /**
   * @return string
   */
  public function getKey(): string
  {
    return $this->key;
  }

  /**
   * @return Carbon
   */
  public function getDepartureDate(): Carbon
  {
    return $this->departure_date;
  }

  /**
   * @return int
   */
  public function getDuration(): int
  {
    return $this->duration;
  }

  /**
   * @return BoardType
   */
  public function getBoardType(): BoardType
  {
    return $this->boardType;
  }

  /**
   * @return int
   */
  public function getOccupancy(): int
  {
    return $this->occupancy;
  }

  /**
   * @return float
   */
  public function getPrice(): float
  {
    return $this->price;
  }

  /**
   * @return string
   */
  public function getTrafficsCode(): string
  {
    return $this->trafficsCode;
  }

  /**
   * @return TravelAgent
   */
  public function getTravelAgents(): TravelAgent
  {
    return $this->travelAgents;
  }


  /**
   * @return array
   */
  public function jsonSerialize(): array
  {
    return [
      'key' => $this->key,
      'trafficsCode' => $this->trafficsCode,
      'departure_date' => $this->departure_date->format('Y-m-d'),
      'duration' => $this->duration,
      'boardType' => $this->boardType->jsonSerialize(),
      'occupancy' => $this->occupancy,
      'price' => $this->price,
      'travelAgent' => $this->travelAgents->jsonSerialize(),
      'room' => $this->room->jsonSerialize(),
      'airport' => $this->airport->jsonSerialize()
    ];
  }
}
