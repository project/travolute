<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

use Carbon\Carbon;

class Accommodation implements \JsonSerializable
{
    /**
     * The accommodation name
     *
     * @var string
     */
    private $name;

    /**
     * The external GIATA identifier
     *
     * @var int
     */
    private $giataId;

    /**
     * The accommodation rating in stars
     *
     * @var int
     */
    private $stars;

    /**
     * The destination model of the accommodation
     *
     * @var Destination
     */
    private $destination;

    /**
     * A collection accommodation images
     *
     * @var ImageCollection
     */
    private $images;

    /**
     * The date the accommodation was last updated by Giata endpoint
     *
     * @var Carbon
     */
    private $dateLastUpdatedByGiata;

    /**
     * TODO: Implement
     *
     * @var null
     */
    private $facts;

    /**
     * A collection with accommodation texts
     *
     * @var AccommodationTextCollection
     */
    private $texts;

    public function __construct(
        string $name,
        int $giataId,
        int $stars,
        ImageCollection $images,
        Destination $destination,
        $facts, // TODO: Make a model for facts
        Carbon $dateLastUpdatedByGiata = null,
        AccommodationTextCollection $texts = null
    ) {
        // Validate the images, must be of type Image
        foreach ($images as $image) {
            if (!$image instanceof Image) {
                throw new \InvalidArgumentException('Image should be of class '.Image::class);
            }
        }

        $this->name = $name;
        $this->giataId = $giataId;
        $this->stars = $stars;
        $this->images = $images;
        $this->destination = $destination;
        $this->dateLastUpdatedByGiata = $dateLastUpdatedByGiata;
        $this->facts = ($facts ? $facts : []);
        $this->texts = $texts;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getGiataId(): int
    {
        return $this->giataId;
    }

    /**
     * @return int
     */
    public function getStars(): int
    {
        return $this->stars;
    }

    /**
     * @return ImageCollection
     */
    public function getImages(): ImageCollection
    {
        return $this->images;
    }

    /**
     * @return Destination
     */
    public function getDestination(): Destination
    {
        return $this->destination;
    }

    /**
     * @return Carbon
     */
    public function getDateLastUpdatedByGiata(): Carbon
    {
        return $this->dateLastUpdatedByGiata;
    }

    /**
     * @return array
     */
    public function getFacts(): array
    {
        return $this->facts;
    }

    /**
     * @return AccommodationTextCollection
     */
    public function getTexts(): AccommodationTextCollection
    {
        return $this->texts;
    }

    public function jsonSerialize(): array
    {
        return [
            'destination' => $this->destination,
            'facts' => $this->facts,
            'giataId' => $this->giataId,
            'images' => $this->images,
            'name' => $this->name,
            'stars' => $this->stars,
            'texts' => $this->texts
        ];
    }
}
