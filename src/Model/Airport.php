<?php
/**
 * Created by PhpStorm.
 * User: killiantimmermans
 * Date: 2019-03-13
 * Time: 10:25
 */

namespace Drupal\travolute\Model;


class Airport
{

    /**
     * The unique key of the Airport Model
     *
     * @var int
     */
    private $id;

    /**
     * The code of the Airport Model
     *
     * @var string
     */
    private $iata;

    /**
     * The name of the Airport Model
     *
     * @var string
     */
    private $name;

    /**
     * The city of the Airport Model
     *
     * @var string
     */
    private $city;

    /**
     * The country of the Airport Model
     *
     * @var string
     */
    private $country;

    /**
     * The latitude of the Airport Model
     *
     * @var float
     */
    private $lat;

    /**
     * The longitude of the Airport Model
     *
     * @var float
     */
    private $long;

    /**
     * The icao of the Airport Model
     *
     * @var string
     */
    private $icao;

    /**
     * TravelAgent constructor.
     *
     * @param $model
     */
    public function __construct(
      $model
    )
    {
        $this->id = $model->id;
        $this->iata = $model->iata;
        $this->name = $model->name;
        $this->city = $model->city;
        $this->country = $model->country;
        $this->lat = $model->lat;
        $this->long = $model->long;
        $this->icao = $model->icao;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIata(): string
    {
        return $this->iata;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @return int
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @return int
     */
    public function getLong(): float
    {
        return $this->long;
    }

    /**
     * @return string
     */
    public function getIcao(): string
    {
        return $this->icao;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
          'id' => $this->id,
          'iata' => $this->iata,
          'name' => $this->name,
          'city' => $this->city,
          'country' => $this->country,
          'lat' => $this->lat,
          'long' => $this->long,
          'icao' => $this->icao
        ];
    }
}
