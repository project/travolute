<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class FilterCountryRegionCity implements JsonSerializable
{
    private $key;
    private $value;
    private $country_id;
    private $country;
    private $region_id;
    private $region;
    private $city_id;
    private $city;

    public function __construct(
        string $key,
        string $value,
        string $country_id,
        string $country,
        string $region_id,
        string $region,
        string $city_id,
        string $city
    )
    {
        $this->key = $key;
        $this->value = $value;
        $this->country_id = $country_id;
        $this->country = $country;
        $this->region_id = $region_id;
        $this->region = $region;
        $this->city_id = $city_id;
        $this->city = $city;
    }

    public function getKey(): string
    {
        return $this->key;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getCountryId(): string
    {
        return $this->country_id;
    }

    public function getCountry(): string
    {
        return $this->country;
    }

    public function getRegionId(): string
    {
        return $this->region_id;
    }

    public function getRegion(): string
    {
        return $this->region;
    }

    public function getCityId(): string
    {
        return $this->city_id;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function jsonSerialize(): array
    {
        return [
            'key' => $this->key,
            'value' => $this->value,
            'country_id' => $this->country_id,
            'country' => $this->country,
            'region_id' => $this->region_id,
            'region' => $this->region,
            'city_id' => $this->city_id,
            'city' => $this->city,
        ];
    }
}
