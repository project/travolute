<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use Carbon\Carbon;

class RenewTokenResponse
{
    /**
     * The received auth token to use
     *
     * @var string
     */
    private $auth_token;

    /**
     * The datetime (converted to Carbon object) when the auth token expires
     *
     * @var Carbon
     */
    private $auth_token_expire;

    /**
     * LoginResponse constructor.
     * @param string $auth_token
     * @param string $auth_token_expire
     */
    public function __construct(string $auth_token, string $auth_token_expire)
    {
        $this->auth_token = $auth_token;
        $this->auth_token_expire = Carbon::createFromTimestamp((int) $auth_token_expire);

        if (!\is_string($this->auth_token) || empty($this->auth_token)) {
            throw new \InvalidArgumentException('Auth token can\'t be empty!');
        }

        if (!$this->auth_token_expire instanceof Carbon) {
            throw new \InvalidArgumentException('Invalid expire time');
        }

        if ($this->auth_token_expire->isPast()) {
            throw new \InvalidArgumentException('Expire time can\t be in the past');
        }
    }

    /**
     * @return string
     */
    public function getAuthToken(): string
    {
        return $this->auth_token;
    }

    /**
     * @return Carbon
     */
    public function getAuthTokenExpire(): Carbon
    {
        return $this->auth_token_expire;
    }
}
