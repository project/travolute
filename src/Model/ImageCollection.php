<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

/**
 * Class ImageCollection
 *
 * @package Drupal\travolute\Model
 */
class ImageCollection implements \IteratorAggregate, \JsonSerializable
{
    /**
     * @var Image[]
     */
    private $images;

    /**
     * ImageCollection constructor
     *
     * @param Image ...$images
     */
    public function __construct(Image ...$images)
    {
        $this->images = $images;
    }

    /**
     * Returns the iterator over the Image[] array
     *
     * @return \ArrayIterator|\Traversable
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->images);
    }

    /**
     * Creates an image collection from travolute accommodation response
     *
     * @param array $images
     * @return ImageCollection
     */
    public static function createFromTravoluteAccommodationResponse(array $images): ImageCollection
    {
        $collection = [];
        foreach ($images as $image) {
            $collection[] = new Image($image->url);
        }

        return new ImageCollection(...$collection);
    }

    public function jsonSerialize()
    {
        return $this->images;
    }
}
