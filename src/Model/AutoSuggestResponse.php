<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class AutoSuggestResponse implements JsonSerializable
{
    /**
     * The received items for the auto suggest
     *
     * @var Accommodation[]
     */
    private $items;

    /**
     * The number of total items found
     *
     * @var integer
     */
    private $responseCount;

    /**
     * AutoSuggestResponse constructor.
     *
     * @param \stdClass $response
     */
    public function __construct(\stdClass $response)
    {
        if (!isset($response->responseCount) || !\is_int($response->responseCount)) {
            throw new \InvalidArgumentException('Invalid responseCount');
        }

        if (!isset($response->items) || !\is_array($response->items)) {
            throw new \InvalidArgumentException('Invalid responseCount');
        }

        $this->items = $this->convertItems($response->items);
        $this->responseCount = $response->responseCount;
    }

    /**
     * Converts the response->items to accommodationModel
     *
     * @param \stdClass[] $items
     * @return Accommodation[]
     */
    private function convertItems(array $items): array
    {
        $accommodationModels = [];
        foreach ($items as $item) {
            $accommodationModels[] = new Accommodation(
                $item->name,
                $item->code,
                $item->stars,
                ImageCollection::createFromTravoluteAccommodationResponse($item->images),
                new Destination(
                    $item->destination->id,
                    $item->destination->city,
                    isset($item->destination->regionId) ?
                        new Region($item->destination->regionId, $item->destination->region) : null,
                    new Country($item->destination->countryId, $item->destination->country)
                ),
              $item->accommodationFacts
            );
        }

        return $accommodationModels;
    }

    /**
     * @return Accommodation[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getResponseCount(): int
    {
        return $this->responseCount;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'items' => $this->items,
            'responseCount' => $this->responseCount,
        ];
    }
}
