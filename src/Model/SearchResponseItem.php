<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class SearchResponseItem implements JsonSerializable
{
    /**
     * @var Accommodation
     */
    private $accommodation;

    /**
     * @var Price
     */
    private $price;

    public function __construct(Accommodation $accommodation, Price $price)
    {
        $this->accommodation = $accommodation;
        $this->price = $price;
    }

    /**
     * @return Accommodation
     */
    public function getAccommodation(): Accommodation
    {
        return $this->accommodation;
    }

    /**
     * @return Price
     */
    public function getPrice(): Price
    {
        return $this->price;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'accommodation' => $this->accommodation,
            'price' => $this->price,
        ];
    }
}
