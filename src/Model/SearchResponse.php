<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class SearchResponse implements JsonSerializable
{
    /**
     * The received items for the search
     *
     * @var SearchResponseItem[]
     */
    private $items;

    /**
     * The number of total items found
     *
     * @var integer
     */
    private $responseCount;

    /**
     * AutoSuggestResponse constructor.
     *
     * @param \stdClass $response
     */
    public function __construct(\stdClass $response) // TODO: Do not use stdClass as parameter input, use a class
    {
        if (!isset($response->responseCount) || !is_numeric($response->responseCount)) {
            throw new \InvalidArgumentException('Invalid responseCount');
        }

        if (!isset($response->items) || !is_array($response->items)) {
            throw new \InvalidArgumentException('Invalid responseCount');
        }

        $this->items = $this->convertItems($response->items);
        $this->responseCount = (int)$response->responseCount;
    }

    /**
     * Converts the response->items to accommodationModel
     *
     * @param \stdClass[] $items
     * @return SearchResponseItem[]
     */
    private function convertItems(array $items): array
    {
        $searchItemModels = [];
        foreach ($items as $item) {
            $searchItemModels[] = new SearchResponseItem(
                new Accommodation(
                    $item->name,
                    $item->code,
                    $item->stars,
                    ImageCollection::createFromTravoluteAccommodationResponse($item->images),
                    new Destination(
                        $item->destination->id,
                        $item->destination->city,
                        isset($item->destination->regionId) ?
                            new Region($item->destination->regionId, $item->destination->region) : null,
                        new Country($item->destination->countryId, $item->destination->country)
                    ),
                    isset($item->accommodationFacts) ? $item->accommodationFacts : []
                ),
                new Price(
                    $item->priceModel->departure_date,
                    $item->priceModel->duration,
                    $item->priceModel->boardtype,
                    $item->priceModel->occupancy,
                    $item->priceModel->price
                )
            );
        }

        return $searchItemModels;
    }

    /**
     * @return Accommodation[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getResponseCount(): int
    {
        return $this->responseCount;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'items' => $this->items,
            'responseCount' => $this->responseCount,
        ];
    }
}
