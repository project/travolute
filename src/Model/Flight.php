<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

class Flight implements \JsonSerializable
{
    private $id;
    private $flightCode;
    private $airlineCode;
    private $airline;
    private $flightNumber;
    private $departureIataCode;
    private $departureDate;
    private $departureTime;
    private $arrivalIataCode;
    private $arrivalDate;
    private $arrivalTime;
    private $classCode;
    private $class;

    /**
     * Flight constructor.
     *
     * @param $id
     * @param $flightCode
     * @param $airlineCode
     * @param $airline
     * @param $flightNumber
     * @param $departureIataCode
     * @param $departureDate
     * @param $departureTime
     * @param $arrivalIataCode
     * @param $arrivalDate
     * @param $arrivalTime
     * @param $classCode
     * @param $class
     */
    public function __construct(
        $id,
        $flightCode,
        $airlineCode,
        $airline,
        $flightNumber,
        $departureIataCode,
        $departureDate,
        $departureTime,
        $arrivalIataCode,
        $arrivalDate,
        $arrivalTime,
        $classCode,
        $class
    ) {
        $this->id = $id;
        $this->flightCode = $flightCode;
        $this->airlineCode = $airlineCode;
        $this->airline = $airline;
        $this->flightNumber = $flightNumber;
        $this->departureIataCode = $departureIataCode;
        $this->departureDate = $departureDate;
        $this->departureTime = $departureTime;
        $this->arrivalIataCode = $arrivalIataCode;
        $this->arrivalDate = $arrivalDate;
        $this->arrivalTime = $arrivalTime;
        $this->classCode = $classCode;
        $this->class = $class;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'flightCode' => $this->flightCode,
            'airlineCode' => $this->airlineCode,
            'airline' => $this->airline,
            'flightNumber' => $this->flightNumber,
            'departureIataCode' => $this->departureIataCode,
            'departureDate' => $this->departureDate,
            'departureTime' => $this->departureTime,
            'arrivalIataCode' => $this->arrivalIataCode,
            'arrivalDate' => $this->arrivalDate,
            'arrivalTime' => $this->arrivalTime,
            'classCode' => $this->classCode,
            'class' => $this->class
        ];
    }
}
