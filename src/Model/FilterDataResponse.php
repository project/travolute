<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

use JsonSerializable;

class FilterDataResponse implements JsonSerializable
{
    public const TYPE_AIRPORT = 'FILTERDATA_TYPE_AIRPORT';
    public const TYPE_REGION = 'FILTERDATA_TYPE_REGION';
    public const TYPE_CITY = 'FILTERDATA_TYPE_CITY';
    public const TYPE_COUNTRY = 'FILTERDATA_TYPE_COUNTRY';
    public const TYPE_BOARDTYPE = 'FILTERDATA_TYPE_BOARDTYPE';
    public const TYPE_DURATION = 'FILTERDATA_TYPE_DURATION';
    public const TYPE_ACCOMMODATION = 'FILTERDATA_TYPE_ACCOMMODATION';

    /**
     * @var Filter[]
     */
    private $airports = [];

    /**
     * @var Filter[]
     */
    private $regions = [];

    /**
     * @var Filter[]
     */
    private $cities = [];

    /**
     * @var Filter[]
     */
    private $countries = [];

    /**
     * @var Filter[]
     */
    private $boardtypes = [];

    /**
     * @var Filter[]
     */
    private $durations = [];

    /**
     * @var Filter[]
     */
    private $accommodations = [];

    /**
     * FilterDataResponse constructor.
     *
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->initializeAirports($response);
        $this->initializeRegions($response);
        $this->initializeCities($response);
        $this->initializeCountries($response);
        $this->initializeBoardtypes($response);
        $this->initializeDurations($response);
        $this->initializeAccommodations($response);
    }

    /**
     * Initialize airports filter
     *
     * @param array $response
     */
    private function initializeAirports(array $response): void
    {
        if (isset($response[self::TYPE_AIRPORT])) {
            foreach ($response[self::TYPE_AIRPORT] as $item) {
                $this->airports[] = new AirportFilter((string)$item->key, (string)$item->value, (float)$item->lat, (float)$item->lng);
            }
        }
    }

    /**
     * Initialize region filter
     *
     * @param array $response
     */
    private function initializeRegions(array $response): void
    {
        if (isset($response[self::TYPE_REGION])) {
            foreach ($response[self::TYPE_REGION] as $item) {
                $this->regions[] = new FilterCountry(
                    (string)$item->key,
                    (string)$item->value,
                    (string)$item->country_id,
                    (string)$item->country
                );
            }
        }
    }

    /**
     * Initialize city filter
     *
     * @param array $response
     */
    private function initializeCities(array $response): void
    {
        if (isset($response[self::TYPE_CITY])) {
            foreach ($response[self::TYPE_CITY] as $item) {
                $this->cities[] = new FilterCountry(
                    (string)$item->key,
                    (string)$item->value,
                    (string)$item->country_id,
                    (string)$item->country
                );
            }
        }
    }

    /**
     * Initialize country filter
     *
     * @param array $response
     */
    private function initializeCountries(array $response): void
    {
        if (isset($response[self::TYPE_COUNTRY])) {
            foreach ($response[self::TYPE_COUNTRY] as $item) {
                $this->countries[] = new Filter((string)$item->key, (string)$item->value);
            }
        }
    }

    /**
     * Initialize boardtype filter
     *
     * @param array $response
     */
    private function initializeBoardtypes(array $response): void
    {
        if (isset($response[self::TYPE_BOARDTYPE])) {
            foreach ($response[self::TYPE_BOARDTYPE] as $item) {
                $this->boardtypes[] = new Filter((string)$item->key, (string)$item->value);
            }
        }
    }

    /**
     * Initialize duration filter
     *
     * @param array $response
     */
    private function initializeDurations(array $response): void
    {
        if (isset($response[self::TYPE_DURATION])) {
            foreach ($response[self::TYPE_DURATION] as $item) {
                $this->durations[] = new Filter((string)$item->key, (string)$item->value);
            }
        }
    }

    /**
     * Initialize accommodation filter
     *
     * @param array $response
     */
    private function initializeAccommodations(array $response): void
    {
        if (isset($response[self::TYPE_ACCOMMODATION])) {
            foreach ($response[self::TYPE_ACCOMMODATION] as $item) {
                $this->accommodations[] = new FilterCountryRegionCity(
                    (string)$item->key,
                    (string)$item->value,
                    (string)$item->country_id,
                    (string)$item->country,
                    (string)$item->region_id,
                    (string)$item->region,
                    (string)$item->city_id,
                    (string)$item->city
                );
            }
        }
    }

    /**
     * @return Filter[]
     */
    public function getAirports(): array
    {
        return $this->airports;
    }

    /**
     * @return Filter[]
     */
    public function getRegions(): array
    {
        return $this->regions;
    }

    /**
     * @return Filter[]
     */
    public function getCities(): array
    {
        return $this->cities;
    }

    /**
     * @return Filter[]
     */
    public function getCountries(): array
    {
        return $this->countries;
    }

    /**
     * @return Filter[]
     */
    public function getBoardtypes(): array
    {
        return $this->boardtypes;
    }

    /**
     * @return Filter[]
     */
    public function getDurations(): array
    {
        return $this->durations;
    }

    /**
     * @return Filter[]
     */
    public function getAccommodations(): array
    {
        return $this->accommodations;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'airports' => $this->airports,
            'regions' => $this->regions,
            'cities' => $this->cities,
            'countries' => $this->countries,
            'boardtypes' => $this->boardtypes,
            'durations' => $this->durations,
            'accommodations' => $this->accommodations,
        ];
    }
}
