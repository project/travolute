<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

class Image implements \JsonSerializable
{
    /**
     * The image url
     *
     * @var string
     */
    private $url;

    /**
     * Image constructor.
     *
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    public function jsonSerialize(): array
    {
        return [
            'url' => $this->url
        ];
    }
}
