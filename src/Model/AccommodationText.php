<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

class AccommodationText implements \JsonSerializable
{
    /**
     * The text titel
     *
     * @var string
     */
    private $title;

    private $description;

    private $language;

    /**
     * AccommodationText constructor.
     *
     * @param string $title
     * @param string $description
     * @param string $language
     */
    public function __construct(string $title, string $description, string $language)
    {
        $this->title = $title;
        $this->description = $description;
        $this->language = $language;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getLanguage(): string
    {
        return $this->language;
    }

    public function jsonSerialize(): array
    {
        return [
            'description' => $this->description,
            'language' => $this->language,
            'title' => $this->title
        ];
    }
}
