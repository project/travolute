<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

class Region implements \JsonSerializable
{
    /**
     * The travolute id of the region
     *
     * @var int
     */
    private $id;
    private $name;

    /**
     * Region constructor.
     *
     * @param int $id
     * @param string $name
     */
    public function __construct(int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function jsonSerialize(): array
    {
        return [
            'name' => $this->name
        ];
    }
}
