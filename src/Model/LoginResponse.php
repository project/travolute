<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use Carbon\Carbon;

class LoginResponse
{
    /**
     * The received auth token to use
     *
     * @var string
     */
    private $auth_token;

    /**
     * The datetime (converted to Carbon object) when the auth token expires
     *
     * @var Carbon
     */
    private $auth_token_expire;

    /**
     * The received regenerate token
     *
     * @var string
     */
    private $regenerate_token;

    /**
     * LoginResponse constructor.
     * @param string $auth_token
     * @param string $auth_token_expire
     * @param string $regenerate_token
     */
    public function __construct(string $auth_token, string $auth_token_expire, string $regenerate_token)
    {
        $this->auth_token = $auth_token;
        $this->auth_token_expire = Carbon::createFromTimestamp((int) $auth_token_expire);
        $this->regenerate_token = $regenerate_token;

        if (!\is_string($this->auth_token) || empty($this->auth_token)) {
            throw new \InvalidArgumentException('Auth token can\'t be empty!');
        }

        if (!\is_string($this->regenerate_token) || empty($this->regenerate_token)) {
            throw new \InvalidArgumentException('Regenerate token can\'t be empty!');
        }

        if (!$this->auth_token_expire instanceof Carbon) {
            throw new \InvalidArgumentException('Invalid expire time');
        }

        if ($this->auth_token_expire->isPast()) {
            throw new \InvalidArgumentException('Expire time can\t be in the past');
        }
    }

    /**
     * @return string
     */
    public function getAuthToken(): string
    {
        return $this->auth_token;
    }

    /**
     * @return Carbon
     */
    public function getAuthTokenExpire(): Carbon
    {
        return $this->auth_token_expire;
    }

    /**
     * @return string
     */
    public function getRegenerateToken(): string
    {
        return $this->regenerate_token;
    }
}
