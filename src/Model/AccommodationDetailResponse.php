<?php
declare(strict_types=1);

namespace Drupal\travolute\Model;

use Carbon\Carbon;
use Drupal\travolute\Model\AccommodationPrice;
use Drupal\travolute\Model\FilterDataResponse;
use Money\Money;
use Money\Currency;

class AccommodationDetailResponse implements \JsonSerializable
{
    /**
     * The accommodation model
     *
     * @var Accommodation
     */
    private $accommodation;

    /**
     * The prices for the requested search query
     *
     * @var AccommodationPrice[]
     */
    private $prices = [];

    /**
     * The prices for the requested search query
     *
     * @var FilterDataResponse[]
     */
    private $filters = [];


    /**
     * AccommodationDetailResponse constructor.
     *
     * Receives the XML from the Travolute API and converts it to Accommodation response model
     *
     * @param \stdClass $response
     */
    public function __construct(\stdClass $response)
    {
        if (!isset($response->accommodation)) {
            throw new \InvalidArgumentException('Invalid accommodation response');
        }

        $this->setAccommodation($response->accommodation);
        $this->setPrices($response->accommodation->prices);
        $this->setFilters($response->filters);

    }

    /**
     * Converts the accommodation part of the response to an accommodation model
     *
     * @param $item: The accommodation part of the accommodation detail response
     * @return void
     */
    private function setAccommodation($item)
    {
        $this->accommodation = new Accommodation(
            $item->name,
            $item->code,
            $item->stars,
            ImageCollection::createFromTravoluteAccommodationResponse($item->accommodationImages),
            new Destination(
                $item->destination->id,
                $item->destination->city,
                isset($item->destination->regionId) ?
                    new Region($item->destination->regionId, $item->destination->region) : null,
                new Country($item->destination->countryId, $item->destination->country)
            ),
            $item->accommodationFacts,
            Carbon::createFromTimestamp($item->lastGiataCheck),
            AccommodationTextCollection::createFromTravoluteAccommodationResponse($item->accommodationTexts)
        );
    }

    /**
     * Creates prices array
     *
     * @param array $prices
     */
    private function setPrices(array $prices)
    {
      // Enforce removal of earlier set prices
      $this->prices = [];

      foreach ($prices as $price) {
        $this->prices[] = new AccommodationPrice(
          $price->key,
          $price->departure_date,
          $price->duration,
          new BoardType($price->boardTypeModel),
          $price->occupancy,
          $price->price,
          new TravelAgent($price->travelAgentModel),
          new Room($price->roomModel),
          new Airport($price->airportModel),
          $price->trafficsCode
        );
      }
    }

    public function groupPricesByDateAndDuration(): void
    {
      $groupedPrices = [];
      foreach ($this->prices as $price) {
        $arrayKey = $price->getDepartureDate()->format('Y-m-d')
          . '_' . $price->getDuration();
        if (!array_key_exists($arrayKey, $groupedPrices)) {
          $groupedPrices[$arrayKey] = [];
        }
        array_push($groupedPrices[$arrayKey], $price);
        // sort keys in group based on price
        usort($groupedPrices[$arrayKey], function ($a, $b) {
          return $a->getPrice() <=> $b->getPrice();
        });
      }
      // Sort groups based on price withing group
      uasort($groupedPrices, function ($a, $b) {
        return $a[0]->getPrice() <=> $b[0]->getPrice();
      });
      $this->prices = $groupedPrices;
    }

    /**
     * @return \Drupal\travolute\Model\Offer[]
     */
    public function getFilters(): array
    {
        return $this->filters->jsonSerialize();
    }

    /**
     * @param FilterDataResponse[] $filters
     */
    public function setFilters(array $filters): void
    {
        $this->filters = new FilterDataResponse($filters);
    }


    /**
     * @return Accommodation
     */
    public function getAccommodation(): Accommodation
    {
        return $this->accommodation;
    }

    /**
     * @return AccommodationPrice[]
     */
    public function getPrices(): array
    {
        return (array)$this->prices;
    }

    public function jsonSerialize(): array
    {
        return [
            'accommodation' => $this->accommodation,
            'prices' => $this->prices,
            'filters' => $this->filters
        ];
    }
}
