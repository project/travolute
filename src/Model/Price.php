<?php

declare(strict_types=1);

namespace Drupal\travolute\Model;

use Carbon\Carbon;
use JsonSerializable;

class Price implements JsonSerializable
{
    /**
     * The departure date of the price model
     *
     * @var Carbon
     */
    private $departure_date;

    /**
     * The duration of the price model
     *
     * @var int
     */
    private $duration;

    /**
     * The board type as string
     *
     * @var string
     */
    private $boardType;

    /**
     * The occupancy (sum of adults and children)
     *
     * @var int
     */
    private $occupancy;

    /**
     * The price
     *
     * @var float
     */
    private $price;

    /**
     * Price constructor.
     *
     * @param string $departure_date
     * @param int $duration
     * @param string $boardType
     * @param int $occupancy
     * @param float $price
     */
    public function __construct(
      string $departure_date,
      int $duration,
      string $boardType,
      $occupancy,
      float $price
    ) {
        if (!preg_match('/^\d{4}-\d{2}-\d{2}$/', $departure_date)) {
            throw new \InvalidArgumentException('Departure date invalid format');
        }

        $this->departure_date = new Carbon($departure_date);
        $this->duration = $duration;
        $this->boardType = $boardType;
        $this->occupancy = ($occupancy ? $occupancy : null);
        $this->price = $price;
    }

    /**
     * @return Carbon
     */
    public function getDepartureDate(): Carbon
    {
        return $this->departure_date;
    }

    /**
     * @return int
     */
    public function getDuration(): int
    {
        return $this->duration;
    }

    /**
     * @return string
     */
    public function getBoardType(): string
    {
        return $this->boardType;
    }

    /**
     * @return int
     */
    public function getOccupancy(): int
    {
        return $this->occupancy;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
          'departure_date' => $this->departure_date->format('Y-m-d'),
          'duration' => $this->duration,
          'boardType' => $this->boardType,
          'occupancy' => $this->occupancy,
          'price' => $this->price
        ];
    }
}

