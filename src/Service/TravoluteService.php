<?php
/**
 * Travolute  - NetAnts
 *
 * @link        http://www.netants.nl
 * @copyright   Copyright (c) NetAnts
 * @license MIT license: http://www.opensource.org/licenses/mit-license.php
 */

declare(strict_types=1);

namespace Drupal\travolute\Service;

use Carbon\Carbon;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\travolute\Model\AccommodationDetailResponse;
use Drupal\travolute\Model\AutoSuggestResponse;
use Drupal\travolute\Model\FilterDataResponse;
use Drupal\travolute\Model\LoginResponse;
use Drupal\travolute\Model\PriceAvailabilityResponse;
use Drupal\travolute\Model\RenewTokenResponse;
use Drupal\travolute\Model\SearchResponse;
use Drupal\travolute\Model\SoapHeaders;
use Drupal\travolute\ValueObject\AccommodationParameters;
use Drupal\travolute\ValueObject\Booking;
use Drupal\travolute\ValueObject\SearchQueryParameters;
use SoapClient;
use SOAPHeader;

class TravoluteService implements TravoluteServiceInterface
{
    /**
     * The SoapClient
     *
     * @var SoapClient
     */
    private $client;

    /**
     * The API key for Travolute XML Api
     *
     * @var string
     */
    private $api_key;

    /**
     * The API secret for Travolute XML Api
     *
     * @var string
     */
    private $api_secret;

    /**
     * The API token received from the Travolute API after successful login
     *
     * @var string
     */
    private $auth_token;

    /**
     * The timestamp when the received API token expires
     *
     * @var Carbon
     */
    private $auth_token_expire;

    /**
     * The token for requesting a renewal
     *
     * @var string
     */
    private $regenerate_token;

    /**
     * The drupal cache to remember auth tokens
     *
     * @var CacheBackendInterface
     */
    private $cache;

    /**
     * The constructor creates a SoapClient based on the given WSDL
     *
     * @param string $api_key
     * @param string $api_secret
     * @param SoapClient $client
     * @param CacheBackendInterface $cache
     */
    public function __construct($api_key, $api_secret, SoapClient $client, CacheBackendInterface $cache)
    {
        $this->client = $client;

        $this->api_key = $api_key;
        $this->api_secret = $api_secret;

        $this->cache = $cache;
    }

    /**
     * Generates a new token based on the regeneration_token if the regeneration_token is not expired
     *
     * @param void
     * @return RenewTokenResponse
     * @deprecated doesn't work, travolute doesn't renew, just log in again
     */
    public function renewToken(): RenewTokenResponse
    {
        $result = $this->client->renewToken($this->regenerate_token);

        return new RenewTokenResponse((string)$result->auth_token, (string)$result->auth_token_expire);
    }

    /**
     * Auto suggest based on query against accommodation name
     *
     * @param string $query
     * @param int $page
     * @return AutoSuggestResponse
     */
    public function autoSuggest(string $query, int $page = 0): AutoSuggestResponse
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();
        $response = $this->client->autosuggest(['query' => $query], $page);
        return new AutoSuggestResponse($response);
    }

    /**
     * Ensures that auth_token is valid for each request.
     * If not logged in, or log in is expired, this method logs in or regenerates token
     *
     * @param void
     * @return void
     */
    public function ensureLoggedIn()
    {
        // If we don't have auth or regenerate tokens, we should login
        if ($this->cache->get('auth_token') === false
            || $this->cache->get('regenerate_token') === false
            || $this->cache->get('auth_token_expire') === false
            || $this->cache->get('auth_token_expire')->data->subMinutes(5) < Carbon::now()) {
            $loginResponse = $this->login();

            // Store in cache for later use
            $this->cache->set('auth_token', $loginResponse->getAuthToken());
            $this->cache->set('auth_token_expire', $loginResponse->getAuthTokenExpire());
            $this->cache->set('regenerate_token', $loginResponse->getRegenerateToken());
        }

        $this->auth_token = $this->cache->get('auth_token')->data;
        /** @var Carbon regenerate_token */
        $this->auth_token_expire = $this->cache->get('auth_token_expire')->data;
        $this->regenerate_token = $this->cache->get('regenerate_token')->data;
    }

    /**
     * Performs a soapclient->login() call based on the api_key and api_secret.
     * On successful login a token and timestamp is returned
     *
     * @param void
     * @return LoginResponse
     */
    public function login(): LoginResponse
    {
        $response = $this->client->login($this->api_key, $this->api_secret);

        return new LoginResponse(
            (string)$response->auth_token,
            (string)$response->auth_token_expire,
            (string)$response->regenerate_token
        );
    }

    /**
     * Creates a standard set of SOAPHeader object and adds it to the SOAPClient
     *
     * @param void
     * @return void
     */
    private function setStandardSoapHeaders()
    {
        // Set authentication headers
        $headerbody = new SoapHeaders(
            $this->cache->get('auth_token')->data,
            $this->cache->get('auth_token_expire')->data->getTimestamp()
        );

        $header = new SOAPHeader("ns1", 'RequestorCredentials', $headerbody);
        $this->client->__setSoapHeaders($header);
    }

    /**
     * Get the globally available filters
     *
     * @param array $items : An array to limit the requested filter items
     * @param array $params : An array of search parameters to limit the filterdata
     * @return FilterDataResponse
     */
    public function getFilters($items, $params): FilterDataResponse
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();
        $response = $this->client->getFilterData($items, $params);
        return new FilterDataResponse($response);
    }

    /**
     * Performs a search action with given params
     *
     * Valid params are:
     *
     *  query           : Free text search
     *  departure_date  : The requested departure date
     *  departure_offset : The deviation from the departure date in days (max +/- 3 days)
     *
     * @param SearchQueryParameters $params : The search parameters to limit the response
     * @param int $page : page number for pagination
     * @param int $maxResults : maximum number of results per page
     * @return SearchResponse
     */
    public function search($params, $page = 0, $maxResults = 15): SearchResponse
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();
        $response = $this->client->search($params->toArray(), $page, $maxResults);
        return new SearchResponse($response);
    }

     /**
     * Performs a searchRange action with given params
     *
     * Valid params are:
     *
     *  query           : Free text search
     *  departure_date  : The requested departure date
     *  return_date     : The requested return date
     *
     * @param SearchQueryParameters $params : The search parameters to limit the response
     * @param int $page : page number for pagination
     * @param int $maxResults : maximum number of results per page
     * @return SearchResponse
     */
    public function searchRange($params, $page = 0, $maxResults = 15): SearchResponse
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();
        $response = $this->client->searchRange($params->toArray(), $page, $maxResults);
        return new SearchResponse($response);
    }


    /**
     * Get accommodation detail and price list for a given query
     *
     * @param               $giataId : The giata code of the accommodation
     * @param AccommodationParameters $params : The search params for filtering the prices
     *
     * @return AccommodationDetailResponse
     */
    public function detail($giataId, AccommodationParameters $params = null): AccommodationDetailResponse
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();
        $response = $this->client->detailAccommodation(
            $giataId,
            $params->toArray()
        );

        return new AccommodationDetailResponse($response);
    }

    /**
     * Get accommodation detail and price list for a given query.
     * Group prices based on accommodations with the same date and travel duration.
     *
     * @param int                        $giataId : The giata code of the accommodation
     * @param AccommodationParameters $parameters : The search params for filtering the prices
     *
     * @return AccommodationDetailResponse
     */
    public function detailWithGroupedPricesBasedOnDateAndDuration (
      int $giataId,
      AccommodationParameters $parameters = null
    ): AccommodationDetailResponse {
      $this->ensureLoggedIn();
      $this->setStandardSoapHeaders();
      $response = $this->client->detailAccommodation(
        $giataId,
        $parameters->toArray()
      );
      $accommodation = new AccommodationDetailResponse($response);
      $accommodation->groupPricesByDateAndDuration();
      return $accommodation;
    }

    /**
     * Check the availability for a given key
     *
     * @param string $key : The key from the priceModel
     * @param array $customers : The customers array
     * @return PriceAvailabilityResponse $results : The result set from SoapService
     */
    public function availability($key, $customers)
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();

        $response = $this->client->checkAvailability(
            $key,
            $customers
        );

        return new PriceAvailabilityResponse($response);
    }
//

    /**
     * Get the preBookResponse for a bookingCode
     *
     * @param string $bookingCode : the booking code
     * @return Booking $results : The result set from SoapService
     */
    public function preBooking($bookingCode)
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();
        $response = $this->client->preBooking($bookingCode);
        return new Booking($response);
    }

    /**
     * Books the actual trip
     *
     * @param string $bookingCode : the booking code
     * @param array $customers : The complete customers array with real names
     * @return Booking $results : The result set from SoapService
     */
    public function booking($bookingCode, array $customers)
    {
        $this->ensureLoggedIn();
        $this->setStandardSoapHeaders();
        $response = $this->client->booking($bookingCode, $customers);
        return new Booking($response);
    }

    /**
     * Prints debug information of the last request / response;
     *
     * @param boolean $format : Format in HTML, XML or RAW?
     * @return void
     */
    public function debugClient($format = null)
    {
        $request = $this->client->__getLastRequest();
        $response = $this->client->__getLastResponse();
        switch ($format) {
            case "xml":
                header("Content-Type: application/xml");
                // Only 1 can be shown in correct XML format
                echo $response;
                break;
            case "html":
                echo "<b>Request:</b><br><pre>" . nl2br(htmlentities(preg_replace("/<(\w+)/", "\n<$1",
                        $request))) . "</pre>";
                echo "<b>Response:</b><br><pre>" . nl2br(htmlentities(preg_replace("/<(\w+)/", "\n<$1",
                        $response))) . "</pre>";
                break;
            case "file":
                $file1 = "data/logs/REQUEST-" . time() . ".xml";
                $file2 = "data/logs/RESPONSE-" . time() . ".xml";
                file_put_contents($file1, $request);
                file_put_contents($file2, $response);
                break;
            default:
                echo "Request: " . PHP_EOL . $request;
                echo "Response: " . PHP_EOL . $response;
                break;
        }
    }

    /**
     * @return string
     */
    public function getAuthToken()
    {
        return $this->auth_token;
    }

    /**
     * @return Carbon
     */
    public function getAuthTokenExpire()
    {
        return $this->auth_token_expire;
    }

    /**
     * @return string
     */
    public function getRegenerateToken()
    {
        return $this->regenerate_token;
    }

}
