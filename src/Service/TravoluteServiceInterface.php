<?php

namespace Drupal\travolute\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\travolute\Model\AccommodationDetailResponse;
use Drupal\travolute\Model\AutoSuggestResponse;
use Drupal\travolute\Model\FilterDataResponse;
use Drupal\travolute\Model\LoginResponse;
use Drupal\travolute\Model\RenewTokenResponse;
use Drupal\travolute\Model\SearchResponse;
use Drupal\travolute\ValueObject\AccommodationParameters;
use Drupal\travolute\ValueObject\SearchQueryParameters;

interface TravoluteServiceInterface
{
    /**
     * The constructor creates a SoapClient based on the given WSDL
     *
     * @param string $api_key
     * @param string $api_secret
     * @param \SoapClient $client
     * @param CacheBackendInterface $cache
     */
    public function __construct($api_key, $api_secret, \SoapClient $client, CacheBackendInterface $cache);

    /**
     * Performs a soapclient->login() call based on the api_key and api_secret.
     * On successful login a token and timestamp is returned
     *
     * @param void
     * @return LoginResponse
     */
    public function login(): LoginResponse;

    /**
     * Ensures that auth_token is valid for each request.
     * If not logged in, of log in is expired, this method logs in or regenerates token
     *
     * @param void
     * @return void
     */
    public function ensureLoggedIn();

    /**
     * Generates a new token based on the regeneration_token if the regeneration_token is not expired
     *
     * @param void
     * @return RenewTokenResponse
     * @deprecated doesn't work, travolute doesn't renew, just log in again
     */
    public function renewToken(): RenewTokenResponse;

    /**
     * Auto suggest based on query against accommodation name
     *
     * @param string $query
     * @return AutoSuggestResponse
     */
    public function autoSuggest(string $query): AutoSuggestResponse;

    /**
     * Get the globally available filters
     *
     * @param array $items : An array to limit the requested filter items
     * @param array $params : An array of search parameters to limit the filterdata
     * @return FilterDataResponse
     */
    public function getFilters($items, $params): FilterDataResponse;

    /**
     * Performs a search action with given params
     *
     * Valid params are:
     *
     *  query           : Free text search
     *  departure_date  : The requested departure date
     *  departure_offset : The deviation from the departure date in days (max +/- 3 days)
     *
     * @param SearchQueryParameters $params : The search parameters to limit the response
     * @param int $page : page number for pagination
     * @param int $maxResults : maximum number of results per page
     * @return SearchResponse
     */
    public function search($params, $page = 0, $maxResults = 15): SearchResponse;

    /**
     * Get accommodation detail and price list for a given query
     *
     * @param               $giataId : The giata code of the accommodation
     * @param AccommodationParameters $params : The search params for filtering the prices
     *
     * @return AccommodationDetailResponse
     */
    public function detail($giataId, AccommodationParameters $params = null): AccommodationDetailResponse;

    /**
     * Get accommodation detail and price list for a given query.
     * Group prices based on accommodations with the same date and travel duration.
     *
     * @param int                        $giataId : The giata code of the accommodation
     * @param AccommodationParameters $parameters : The search params for filtering the prices
     *
     * @return AccommodationDetailResponse
     */
    public function detailWithGroupedPricesBasedOnDateAndDuration (int $giataId, AccommodationParameters $parameters = null): AccommodationDetailResponse;

    /**
     * Check the availability for a given key
     *
     * @param string $key : The key from the priceModel
     * @param array $customers : The customers array
     * @return $results : The result set from SoapService
     */
    public function availability($key, $customers);

    /**
     * Get the preBookResponse for a bookingCode
     *
     * @param string $bookingCode : the booking code
     * @return $results : The result set from SoapService
     */
    public function preBooking($bookingCode);

    /**
     * Books the actual trip
     *
     * @param string $bookingCode : the booking code
     * @param array $customers : The complete customers array with real names
     * @return  $results : The result set from SoapService
     */
    public function booking($bookingCode, array $customers);

    /**
     * Prints debug information of the last request / response;
     *
     * @param boolean $format : Format in HTML, XML or RAW?
     * @return void
     */
    public function debugClient($format = null);
}
